# Changelog

## Version 2.0.2

* Make sure Swift Package builds when client app targets older iOS versions.
* Bump Swift Package version from 5.6 -> 5.7.

## Version 2.0.1

* Client apps no longer needs to specify other API keys than Kindly bot key.
* Swift Package configuration improvements.

## Version 2.0.0

* Initial release
