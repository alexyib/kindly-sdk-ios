// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "KindlySDK",
    platforms: [
        .iOS(.v13),
        .macOS("99.0")
    ],
    products: [
        .library(
            name: "KindlySDK",
            targets: ["KindlySDK"]),
    ],
    dependencies: [
        .package(
            url: "https://github.com/pusher/pusher-websocket-swift.git",
            exact: "8.0.0"
        ),
        .package(
            url: "https://github.com/kirualex/SwiftyGif.git",
            exact: "5.4.3"
        ),
        .package(
            url: "https://github.com/auth0/JWTDecode.swift",
            exact: "3.1.0"
        ),
        .package(
            url: "https://github.com/google/promises.git",
            exact: "2.3.1"
        )
    ],
    targets: [
        .target(
            name: "KindlySDK",
            dependencies: [
                .product(
                    name: "PusherSwift",
                    package: "pusher-websocket-swift",
                    condition: .when(platforms: [.iOS])
                ),
                .product(
                    name: "SwiftyGif",
                    package: "SwiftyGif",
                    condition: .when(platforms: [.iOS])
                ),
                .product(
                    name: "JWTDecode",
                    package: "JWTDecode.swift",
                    condition: .when(platforms: [.iOS])
                ),
                .product(
                    name: "FBLPromises",
                    package: "promises",
                    condition: .when(platforms: [.iOS])
                ),
                .product(
                    name: "Promises",
                    package: "promises",
                    condition: .when(platforms: [.iOS])
                ),
            ],
            resources: [.process("Resources")]
        ),
        .testTarget(
            name: "KindlySDK-Tests",
            dependencies: ["KindlySDK"]),
    ]
)
