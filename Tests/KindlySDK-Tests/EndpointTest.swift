import XCTest
@testable import KindlySDK

@available(iOS 14.0, *)
final class EndpointTest: XCTestCase {
    func testConnectEndpointURLGeneration() throws {
        let correctURL = URL(string: "https://bot.kindly.ai/chatbubble/v2/connect")!
        let connectURL = API.url(for: .connect)
        XCTAssertEqual(connectURL, correctURL)
    }
    
    func testNewEndpoint() throws {
        let correctURL = URL(string: "https://bot.kindly.ai/chatbubble/v2/settings/bot")
        let endpoint = Endpoint(path: "settings/bot", method: .GET)
        let endpointURL = API.url(for: endpoint)
        XCTAssertEqual(endpointURL, correctURL)
    }
    
    func testSettingsURL() throws {
        let botKey = "0001"
        let correctURL = URL(string: "https://chat.kindlycdn.com/settings/\(botKey).json")!
        let settingsURL = Endpoint.settingsURL(for: botKey)
        XCTAssertEqual(settingsURL, correctURL)
    }
}
