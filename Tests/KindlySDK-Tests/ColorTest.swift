import XCTest
@testable import KindlySDK

final class ColorTest: XCTestCase {
    func testHEXConversionBlue() throws {
        // #0000FF should match the following RGB: 0, 0, 255
        let blue = UIColor(hex: "#0000FF")!
        let ciColor = CIColor(color: blue)
        XCTAssertEqual(ciColor.red, 0)
        XCTAssertEqual(ciColor.green, 0)
        XCTAssertEqual(ciColor.blue, 1.0)
    }

    func testHEXConversionRed() throws {
        // #FF0000 should match the following RGB: 255, 0, 0
        let red = UIColor(hex: "#FF0000")!
        let ciColor = CIColor(color: red)
        XCTAssertEqual(ciColor.red, 1.0)
        XCTAssertEqual(ciColor.green, 0)
        XCTAssertEqual(ciColor.blue, 0)
    }
}
