import Foundation

@available(iOS 14.0, *)
final class DownloadChatPresenter {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    private weak var view: DownloadChatView?
    private let httpClient: HTTPClientProtocol
    private let format: DownloadFormat
    
    init(view: DownloadChatView, httpClient: HTTPClientProtocol, format: DownloadFormat) {
        self.view = view
        self.httpClient = httpClient
        self.format = format
    }
    
    func start() {
        downloadChatConversation()
    }
    
    private func downloadChatConversation() {
        guard let token = kindlyChatClient.token else {
            view?.presentErrorAlert()
            return
        }
        
        view?.setLoadingIndicator(active: true)
        
        let headers: [HTTPHeader] = [
            HTTPHeader.kindlyChatBubbleToken(value: token),
        ]
        
        Task {
            do {
                try await httpClient.download(
                    from: API.url(for: format.endpoint),
                    headers: headers,
                    saveToURL: savedFileDirectory
                )
                view?.loadConversationPreview(url: savedFileDirectory)
                view?.setShareButtonVisible()
                view?.setLoadingIndicator(active: false)
            } catch {
                view?.presentErrorAlert()
                view?.setLoadingIndicator(active: false)
            }
        }
    }
    
    /// File location where exported conversation is saved to disk.
    ///
    /// The conversation must be saved to disk to support sharing from `UIActivityViewController`.
    private var savedFileDirectory: URL {
        let cacheDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        return cacheDirectory.appendingPathComponent("downloaded_chat" + format.fileExtension)
    }
    
    // MARK: User Actions
    
    func didPressOpenShareSheet() {
        view?.presentShareSheet(activityItems: [savedFileDirectory])
    }
}
