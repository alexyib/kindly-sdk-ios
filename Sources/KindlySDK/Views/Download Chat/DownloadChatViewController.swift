import UIKit
import WebKit

protocol DownloadChatView: AnyObject {
    func loadConversationPreview(url: URL)
    func setShareButtonVisible()
    func setLoadingIndicator(active: Bool)
    func presentShareSheet(activityItems: [Any])
    func presentErrorAlert()
}

/// Load and preview a downloaded conversation in a web view.
///
/// The user can use the system share dialog (`UIActivityViewController`) to share the downloaded conversation.
@available(iOS 14.0, *)
final class DownloadChatViewController: UIViewController {
    private let webView = WKWebView()
    private var shareButton: UIBarButtonItem!
    private var activityIndicatorView = UIActivityIndicatorView(style: .large)
    
    private var presenter: DownloadChatPresenter!
    private let format: DownloadFormat
        
    init(format: DownloadFormat) {
        self.format = format
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = DownloadChatPresenter(
            view: self,
            httpClient: HTTPClient(),
            format: format
        )
        title = .localizedText(key: .downloadButton)
        view.backgroundColor = .white
        view.add(views: webView, activityIndicatorView)
        webView.backgroundColor = .clear
        webView.allowsLinkPreview = false
        addConstraints()
        addBarButtonItem()
        presenter.start()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            activityIndicatorView.centerXAnchor.constraint(equalTo: webView.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: webView.centerYAnchor),
        ])
    }
    
    private func addBarButtonItem() {
        shareButton = UIBarButtonItem(systemItem: .action, primaryAction: UIAction(handler: { [weak self] _ in
            self?.presenter.didPressOpenShareSheet()
        }))
    }
}

@available(iOS 14.0, *)
extension DownloadChatViewController: DownloadChatView {
    func loadConversationPreview(url: URL) {
        DispatchQueue.main.async {
            self.webView.loadFileURL(url, allowingReadAccessTo: url)
        }
    }
    
    func setShareButtonVisible() {
        DispatchQueue.main.async {
            self.navigationItem.rightBarButtonItem = self.shareButton
        }
    }
    
    func setLoadingIndicator(active: Bool) {
        DispatchQueue.main.async {
            if active {
                self.activityIndicatorView.startAnimating()
            } else {
                self.activityIndicatorView.stopAnimating()
            }
            
        }
    }
    
    func presentShareSheet(activityItems: [Any]) {
        DispatchQueue.main.async {
            let shareSheet = UIActivityViewController(
                activityItems: activityItems,
                applicationActivities: nil
            )
            shareSheet.popoverPresentationController?.barButtonItem = self.shareButton
            self.present(shareSheet, animated: true)
        }
    }
    
    func presentErrorAlert() {
        DispatchQueue.main.async {
            let alertController = UIAlertController(
                title: "Something went wrong",
                message: "Failed to download the conversation history.",
                preferredStyle: .alert
            )
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alertController, animated: true)
        }
    }
}
