enum DownloadFormat {
    case html
    case json
    
    var endpoint: Endpoint {
        switch self {
        case .html: return .exportHTML
        case .json: return .exportJSON
        }
    }
    
    var fileExtension: String {
        switch self {
        case .html: return ".html"
        case .json: return ".json"
        }
    }
}
