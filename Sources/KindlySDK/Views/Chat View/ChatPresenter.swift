import Foundation
import Network
import UIKit.UIImage

@available(iOS 14.0, *)
final class ChatPresenter {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient
    
    private weak var view: ChatView?
    private let attachmentService: AttachmentServiceProtocol

    init(view: ChatView, attachmentService: AttachmentServiceProtocol) {
        self.view = view
        self.attachmentService = attachmentService
        monitorNetworkStatus()
    }
    
    var messageDelay: Int {
        return kindlyChatClient.currentBot?.typingDuration ?? 0
    }
    
    // MARK: User Actions
    
    func didPressSendMessage(text: String) {
        let message = text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !message.isEmpty else {
            return
        }
        view?.clearInputView()

        Task {
            do {
                let userMessage = UserMessage(message: message)
                try await kindlyChatClient.sendMessage(userMessage)
            } catch {
                view?.presentAlert(
                    title: "Failed to send message",
                    message: "The message could not be delivered."
                )
                view?.setTypingIndicatorVisible(false, scrollTo: false)
            }
        }
        
        if messageDelay > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.view?.setTypingIndicatorVisible(true, scrollTo: true)
            }
        }
    }
    
    func didPressEndChat() {
        kindlyChatClient.endActiveChat()
        view?.dismiss()
    }
    
    /// Handle button press when user taps on a AttachmentCell
    func didPressAttachmentCell(_ attachmentCellState: AttachmentCellState?) {
        guard let state = attachmentCellState else {
            return
        }
        
        switch state {
        case .content(let attachment):
            switch attachment.format {
            case .jpeg, .png:
                downloadAndPreviewImageAttachment(attachment)
            case .pdf, .txt, .unsupported:
                view?.presentFilePreview(attachment: attachment)
            }
        case .uploadFailed(let request):
            uploadFile(data: request.payload, filename: request.filename, format: request.format)
        case .uploading:
            break
        }
    }
    
    func didSelectImage(data: Data?, suggestedName: String? = nil, format: FileFormat) {
        guard let data = data else {
            view?.presentAlert(title: "Failed to upload image", message: nil)
            return
        }

        let filename: String
        if let suggestedName = suggestedName {
            filename = suggestedName.lowercased() + format.fileExtension
        } else {
            filename = "image-\(UUID().uuidString.lowercased())" + format.fileExtension
        }
        
        uploadFile(data: data, filename: filename, format: format)
    }
    
    func didSelectFile(at url: URL) {
        do {
            let documentData = try Data(contentsOf: url)
            let filename = url.lastPathComponent
            let format = FileFormat(rawValue: url.pathExtension) ?? .unsupported
            
            uploadFile(data: documentData, filename: filename, format: format)
        } catch {
            view?.presentAlert(title: "Failed to upload file", message: nil)
        }
    }

    // MARK: Attachments
    
    private func uploadFile(data: Data, filename: String, format: FileFormat) {
        view?.insertAttachmentCell(
            state: .uploading(previewImage: UIImage(data: data))
        )
        view?.scrollToLastRow(scrollPosition: .bottom, animated: true)

        let request = AttachmentRequest(payload: data, filename: filename, format: format)
        let botID = kindlyChatClient.botID!
        let chatID = kindlyChatClient.chatID!
        let token = kindlyChatClient.token!
        
        Task {
            do {
                try await attachmentService.upload(request: request, botID: botID, chatID: chatID, token: token)
                view?.playHapticFeedback(type: .success)
            } catch {
                view?.insertAttachmentCell(state: .uploadFailed(retryRequest: request))
                view?.clearAttachmentPlaceholderCells()
                view?.playHapticFeedback(type: .error)
            }
        }
    }
    
    private func downloadAndPreviewImageAttachment(_ attachment: Attachment) {
        Task {
            let token = kindlyChatClient.token!
            guard let data = try await attachmentService.download(
                storagePath: attachment.storagePath,
                token: token
            )
            else {
                return
            }
            view?.presentImagePreview(image: .data(data))
        }
    }
    
    // MARK: Chat interaction
    
    private func handleButtonPress(type: ButtonType, value: String) {
        switch type {
        case .email:
            guard let mailURL = URL(string: "mailto:\(value)") else {
                return
            }
            view?.openURL(mailURL)
        case .link:
            view?.openSafari(url: URL(string: value)!)
        case .phone:
            guard let callURL = URL(string: "tel://\(value)") else {
                break
            }
            view?.openURL(callURL)
        case .takeoverRequest:
            view?.setTypingIndicatorVisible(true, scrollTo: true)
            NotificationCenter.default.post(name: NSNotification.Name("KindlySDK-Handover"), object: nil)
        case .uploadAttachment:
            view?.endEditing()
            view?.presentSelectAttachmentSource()
        case .abortFollowup, .dialogueTrigger, .quickReply:
            view?.setTypingIndicatorVisible(true, scrollTo: true)
        case .suggestionDialogueTrigger, .unknown:
            break
        }
    }
    
    // MARK: Convert ChatMessage to ChatRow objects

    /// Convert to ChatRow elements the UITableView can display.
    /// The ChatMessage object should be added before buttons or images.
    func convertChatMessageToChatRow(_ message: ChatMessage) -> [ChatRow] {
        var convertedMessages = [ChatRow]()
        
        let attachments = message.attachments
        let buttons = message.buttons
        let images = message.imagesCarousel
        
        if message.text != nil {
            convertedMessages.append(.message(message))
        }
        
        if buttons.count > 0 {
            convertedMessages.append(.buttonGroup(buttons: buttons, message: message))
        }

        if images.count > 0 {
            convertedMessages.append(.imageGroup(images: images, size: message.imageCarouselSize, id: message.id))
        }
        
        for attachment in attachments {
            convertedMessages.append(.attachment(attachmentState: .content(attachment: attachment)))
        }
         
        if let videoURL = message.videoSource {
            convertedMessages.append(.video(url: videoURL, id: message.id + videoURL.absoluteString))
        }
            
        return convertedMessages
    }
        
    func convertAllMessagesToChatRows() -> [ChatRow] {
        var chatRows = [ChatRow]()
        kindlyChatClient.messages.sorted().forEach { chatMessage in
            let convertedChatRow = convertChatMessageToChatRow(chatMessage)
            
            for row in convertedChatRow {
                chatRows.append(row)
            }
        }
        return chatRows
    }
    
    // MARK: Subscribe to network status events
    
    private func monitorNetworkStatus() {
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "monitoring")
        monitor.start(queue: queue)
               
        monitor.pathUpdateHandler = { [weak self] path in
            switch path.status {
            case .satisfied:
                self?.view?.setNoInternetViewVisible(false)
            case .unsatisfied:
                self?.view?.setNoInternetViewVisible(true)
            case .requiresConnection:
                break
            @unknown default:
                break
            }
        }
    }
}

// MARK: Chat Interaction delegate

@available(iOS 14.0, *)
extension ChatPresenter: ChatInteractionDelegate {
    func didPressButton(chatButton: ChatButton, chatMessage: ChatMessage) {
        let buttonValue = chatButton.value ?? ""
        
        let chatButtonDTO = ButtonInteractionDTO.ButtonClickDTO(
            id: chatButton.id,
            buttonType: chatButton.type,
            value: buttonValue
        )
        let chatMessageDTO = ButtonInteractionDTO.ChatMessageDTO(
            id: chatMessage.id,
            sender: chatMessage.sender
        )
        handleButtonPress(type: chatButton.type, value: buttonValue)
        
        Task {
            do {
                // throw KindlySDKError.configNotSet

                try await kindlyChatClient.submitButtonSelection(
                    buttonInteraction: ButtonInteractionDTO(
                        button: chatButtonDTO,
                        chatMessage: chatMessageDTO
                    )
                )
            } catch {
                view?.presentAlert(
                    title: "Failed to send message",
                    message: nil
                )
                view?.setTypingIndicatorVisible(false, scrollTo: false)
            }
        }
    }

    func didPressImagePreview(type: ImagePreview) {
        view?.presentImagePreview(image: type)
    }

    func didPressLink(url: URL) {
        view?.openSafari(url: url)
    }

    func didPressAttachmentCell(attachment: AttachmentCellState?) {
        didPressAttachmentCell(attachment)
    }
    
    func didPressImagePreview(imageData: Data) {
        view?.presentImagePreview(image: .data(imageData))
    }
}
