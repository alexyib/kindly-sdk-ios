import Combine
import OSLog
import PhotosUI
import SafariServices
import UIKit

protocol ChatView: AnyObject {
    func openURL(_ url: URL)
    func openSafari(url: URL)
    func scrollToLastRow(scrollPosition: UITableView.ScrollPosition, animated: Bool)
    func setNoInternetViewVisible(_ visible: Bool)
    func setTypingIndicatorVisible(_ visible: Bool, scrollTo: Bool)
    func insertAttachmentCell(state: AttachmentCellState)
    func clearAttachmentPlaceholderCells()
    func presentSelectAttachmentSource()
    func presentPhotoPicker()
    func presentFilePicker()
    func presentImagePreview(image: ImagePreview)
    func presentFilePreview(attachment: Attachment)
    func presentAlert(title: String, message: String?)
    func presentChatSettings()
    func playHapticFeedback(type: UINotificationFeedbackGenerator.FeedbackType)
    func clearInputView()
    func endEditing()
    func dismiss()
}

protocol ChatInteractionDelegate: AnyObject {
    func didPressButton(chatButton: ChatButton, chatMessage: ChatMessage)
    func didPressImagePreview(type: ImagePreview)
    func didPressLink(url: URL)
    func didPressAttachmentCell(attachment: AttachmentCellState?)
}

// MARK: ChatViewController

@available(iOS 14.0, *)
final class ChatViewController: UIViewController {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    private var presenter: ChatPresenter!
    private lazy var dataSource = makeDataSource()
    private var cancellables = Set<AnyCancellable>()
    
    // MARK: UI Components
    
    private let tableView = UITableView(frame: .zero, style: .plain)
    private let inputTextView = InputTextView()

    private lazy var toolbarView: UIView = {
        let view = UIView()
        view.backgroundColor = ThemeManager.shared.currentTheme.background
        return view
    }()

    private lazy var toolbarBorder: UIView = {
        let view = UIView()
        view.backgroundColor = .black.withAlphaComponent(0.1)
        view.isUserInteractionEnabled = false
        return view
    }()

    private lazy var submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.accessibilityLabel = "Send"
        button.setTitle(" ", for: .normal) // Workaround for increasing tap area.
        button.setImage(UIImage(named: "icon_send", in: .module, with: nil)?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageEdgeInsets.left = ViewConstants.toolbarSpacerMargin
        button.imageEdgeInsets.right = UIConstants.horizontalMargin
        button.isHidden = true
        return button
    }()
    
    private let noInternetView: NoInternetView = {
        let view = NoInternetView()
        view.isHidden = true
        return view
    }()
    
    // MARK: Custom constraints
    
    /// The toolbar view's bottom constraint.
    private var toolbarBottomConstraint: NSLayoutConstraint!
    
    /// InputView will be full width.
    private var pinInputViewToEdge: NSLayoutConstraint!
    
    /// InputView will be pinned to the submit button on the right side.
    private var pinInputViewToSubmitButton: NSLayoutConstraint!
    
    // MARK: View lifecycle and initial config
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = ThemeManager.shared.currentTheme.background
        view.add(views: tableView, toolbarView, noInternetView)
        toolbarView.add(views: inputTextView, submitButton, toolbarBorder)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        presenter = ChatPresenter(view: self, attachmentService: AttachmentService())
        navigationItem.backButtonDisplayMode = .minimal

        addConstraints()
        configureTableView()
        addNavigationBarButtons()
        addActionsAndDelegates()
        addKeyboardObservers()
        subscribeToBot()
        
        // If the conversation already contains messages, we display them directly.
        if kindlyChatClient.messages.count > 0 {
            configureInitialDataSource()
            scrollToLastRow(scrollPosition: .bottom, animated: false)
            subscribeToChatEvents()
        } else {
            // If this is a new conversation, we display a typing indicator and delay the
            // presentation of messages.
            setTypingIndicatorVisible(true, scrollTo: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.subscribeToChatEvents()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func addConstraints() {
        toolbarBottomConstraint = toolbarView.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor,
            constant: -ViewConstants.bottomMargin
        )
        
        pinInputViewToSubmitButton = inputTextView.trailingAnchor.constraint(equalTo: submitButton.leadingAnchor)

        pinInputViewToEdge = inputTextView.trailingAnchor.constraint(
            equalTo: toolbarView.trailingAnchor,
            constant: -UIConstants.horizontalMargin
        )
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor),
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            toolbarView.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            toolbarBottomConstraint,
            toolbarView.widthAnchor.constraint(equalTo: view.widthAnchor),
            toolbarView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            inputTextView.topAnchor.constraint(equalTo: toolbarView.topAnchor, constant: ViewConstants.inputViewVerticalMargin),
            inputTextView.bottomAnchor.constraint(equalTo: toolbarView.bottomAnchor, constant: -ViewConstants.inputViewVerticalMargin),
            inputTextView.leadingAnchor.constraint(equalTo: toolbarView.leadingAnchor, constant: UIConstants.horizontalMargin),
            pinInputViewToEdge,
            inputTextView.centerYAnchor.constraint(equalTo: toolbarView.centerYAnchor),
            
            submitButton.bottomAnchor.constraint(equalTo: inputTextView.bottomAnchor),
            submitButton.trailingAnchor.constraint(equalTo: toolbarView.trailingAnchor),
            submitButton.widthAnchor.constraint(equalToConstant: ViewConstants.submitButtonSize + ViewConstants.toolbarSpacerMargin),
            submitButton.heightAnchor.constraint(equalToConstant: 40),
            
            toolbarBorder.topAnchor.constraint(equalTo: toolbarView.topAnchor),
            toolbarBorder.widthAnchor.constraint(equalTo: toolbarView.widthAnchor),
            toolbarBorder.heightAnchor.constraint(equalToConstant: 1),
            toolbarBorder.centerXAnchor.constraint(equalTo: toolbarView.centerXAnchor),
            
            noInternetView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            noInternetView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            noInternetView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            noInternetView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
    
    private func addNavigationBarButtons() {
        let settingsButton = UIBarButtonItem(
            title: "Menu",
            image: UIImage(systemName: "ellipsis.circle"),
            primaryAction: nil,
            menu: createMenu()
        )
        navigationItem.leftBarButtonItem = settingsButton

        let closeButton = UIBarButtonItem(
            title: "Close",
            image: UIImage(systemName: "chevron.down"),
            primaryAction: UIAction(handler: { [weak self] _ in
                self?.dismiss(animated: true)
            })
        )
        navigationItem.rightBarButtonItem = closeButton
    }
    
    private func createMenu() -> UIMenu {
        return UIMenu(children: [
            UIAction(title: .localizedText(key: .settingsButton)) { [weak self] _ in
                self?.presentChatSettings()
            },
            UIAction(
                title: .localizedText(key: .cancelChatButton),
                image: nil,
                attributes: [.destructive]
            ) { [weak self] _ in
                self?.presenter.didPressEndChat()
            },
        ])
    }
    
    private func configureTableView() {
        tableView.dataSource = dataSource
        tableView.backgroundColor = ThemeManager.shared.currentTheme.background
        tableView.contentInset.top = ViewConstants.verticalContentInset
        tableView.contentInset.bottom = ViewConstants.verticalContentInset
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableView.dragInteractionEnabled = false
        if #available(iOS 15.0, *) {
            tableView.isPrefetchingEnabled = false
        }
        tableView.register(
            AttachmentCell.self,
            ButtonGroupCell.self,
            ImageCarouselCell.self,
            IncomingMessageCell.self,
            SentMessageCell.self,
            TypingIndicatorCell.self,
            VideoCell.self
        )
    }

    private func addActionsAndDelegates() {
        submitButton.addAction(UIAction(handler: { [weak self] _ in
            self?.didPressSendMessage()
        }), for: .primaryActionTriggered)
        inputTextView.textStorage.delegate = self
    }
    
    private func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func configureInitialDataSource() {
        var snapshot = dataSource.snapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(
            presenter.convertAllMessagesToChatRows(),
            toSection: .main
        )
        DispatchQueue.main.async {
            self.dataSource.apply(snapshot, animatingDifferences: false)
        }
    }
    
    // MARK: Subscribe to incoming messages

    private func subscribeToChatEvents() {
        kindlyChatClient.$messages
            .debounce(for: .milliseconds(presenter.messageDelay), scheduler: DispatchQueue.main)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                var snapshot = self?.dataSource.snapshot()
                snapshot?.deleteAllItems()
                snapshot?.appendSections([.main])
                
                if let convertedMessages = self?.presenter.convertAllMessagesToChatRows() {
                    snapshot?.appendItems(convertedMessages, toSection: .main)
                }
                
                if let snapshot = snapshot {
                    self?.dataSource.apply(snapshot, animatingDifferences: true)
                    self?.scrollToLastRow(scrollPosition: .top, animated: true)
                }
            }
            .store(in: &cancellables)

        // MARK: Subscribe to sent messages
        
        kindlyChatClient.$messages
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let newestMessage = self?.kindlyChatClient.messages.last else {
                    return
                }
                
                guard newestMessage.isFromUser else {
                    return
                }
                
                guard newestMessage.text != nil else {
                    return
                }
                
                var snapshot = self?.dataSource.snapshot()
                snapshot?.appendItems([.message(newestMessage)], toSection: .main)
                if let snapshot = snapshot {
                    self?.dataSource.apply(snapshot, animatingDifferences: true)
                    self?.scrollToLastRow(scrollPosition: .top, animated: true)
                }
            }
            .store(in: &cancellables)
    }
    
    private func subscribeToBot() {
        kindlyChatClient.$currentBot
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.setupNavigationBarHeader()
            }
            .store(in: &cancellables)
    }
    
    private func setupNavigationBarHeader() {
        let viewTitle = kindlyChatClient.currentBot?.name ?? ""
        
        guard let avatarURL = kindlyChatClient.currentBot?.avatarURL else {
            // If no avatar is provided, we only set the title
            title = viewTitle
            return
        }
        
        AsyncImageService().fetchImage(for: avatarURL) { result in
            switch result {
            case .success(let image):
                if let image = image {
                    self.navigationItem.setImageTitleView(
                        image: image,
                        title: viewTitle
                    )
                }
            case .failure:
                Logger.shared.log(level: .debug, "Error fetching bot avatar")
            }
        }
    }
    
    // MARK: Setup Data Source
    
    private func makeDataSource() -> DataSource {
        let dataSource = DataSource(tableView: tableView) { [weak self] tableView, indexPath, item in
            switch item {
            case .attachment(let attachmentState):
                let cell = tableView.dequeueReusableCell(withIdentifier: AttachmentCell.identifier, for: indexPath) as! AttachmentCell
                cell.delegate = self?.presenter
                cell.state = attachmentState
                return cell
            case .buttonGroup(let buttons, let message):
                let cell = tableView.dequeueReusableCell(withIdentifier: ButtonGroupCell.identifier, for: indexPath) as! ButtonGroupCell
                cell.delegate = self?.presenter
                cell.update(buttons: buttons, message: message)
                return cell
            case .imageGroup(let images, let size, _):
                let cell = tableView.dequeueReusableCell(withIdentifier: ImageCarouselCell.identifier, for: indexPath) as! ImageCarouselCell
                cell.delegate = self?.presenter
                cell.update(images: images, size: size)
                return cell
            case .message(let message):
                if message.isFromUser {
                    let cell = tableView.dequeueReusableCell(withIdentifier: SentMessageCell.identifier, for: indexPath) as! SentMessageCell
                    cell.update(message: message)
                    return cell
                }
                
                let cell = tableView.dequeueReusableCell(withIdentifier: IncomingMessageCell.identifier, for: indexPath) as! IncomingMessageCell
                cell.update(message: message)
                return cell
            case .typingPlaceholder:
                return TypingIndicatorCell()
            case .video(let url, _):
                let cell = tableView.dequeueReusableCell(withIdentifier: VideoCell.identifier, for: indexPath) as! VideoCell
                cell.delegate = self?.presenter
                cell.update(url: url)
                return cell
            }
        }
        dataSource.defaultRowAnimation = .fade
        return dataSource
    }
    
    // MARK: Scrolling
    
    private func scrollToRow(at indexPath: IndexPath, scrollPosition: UITableView.ScrollPosition, animated: Bool) {
        DispatchQueue.main.async {
            guard self.tableView.canScrollTo(indexPath: indexPath) else {
                return
            }
            self.tableView.scrollToRow(at: indexPath, at: scrollPosition, animated: animated)
        }
    }
    
    // MARK: User Actions
    
    @objc private func didPressSendMessage() {
        presenter.didPressSendMessage(text: inputTextView.text)
        submitButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.submitButton.isEnabled = true
        }
    }
    
    // MARK: Keyboard
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect) else {
            return
        }
        
        // Make sure the keyboard also scales correctly on iPad by converting view height.
        let constant = view.bounds.height - max(0, view.convert(keyboardSize, from: nil).origin.y)
        toolbarBottomConstraint.constant = -(constant - view.safeAreaInsets.bottom)
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0.25
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
            self.scrollToLastRow(scrollPosition: .bottom, animated: true)
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        toolbarBottomConstraint.constant = -ViewConstants.bottomMargin
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0.25
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }
    
    override var keyCommands: [UIKeyCommand]? {
        return [UIKeyCommand(
            action: #selector(didPressSendMessage),
            input: "\r",
            modifierFlags: .command,
            discoverabilityTitle: "Send message"
        )]
    }
}

// MARK: ChatView implementation

@available(iOS 14.0, *)
extension ChatViewController: ChatView {
    func openURL(_ url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            presentAlert(title: "Failed to open link", message: url.absoluteString)
        }
    }

    func openSafari(url: URL) {
        let safariVC = SFSafariViewController(url: url)
        present(safariVC, animated: true)
    }
    
    func scrollToLastRow(scrollPosition: UITableView.ScrollPosition, animated: Bool) {
        DispatchQueue.main.async {
            guard let lastIndex = self.tableView.lastIndexPath else {
                return
            }
            self.scrollToRow(at: lastIndex, scrollPosition: scrollPosition, animated: animated)
        }
    }
    
    func setNoInternetViewVisible(_ visible: Bool) {
        DispatchQueue.main.async {
            self.noInternetView.isHidden = !visible
        }
    }
    
    func setTypingIndicatorVisible(_ visible: Bool, scrollTo: Bool) {
        DispatchQueue.main.async {
            var snapshot = self.dataSource.snapshot()
            if visible {
                // Make sure typing indicator is not already displayed.
                if snapshot.indexOfSection(.typingIndicator) == nil {
                    snapshot.appendSections([.typingIndicator])
                    snapshot.appendItems([.typingPlaceholder], toSection: .typingIndicator)
                }
            } else {
                if snapshot.indexOfSection(.typingIndicator) != nil {
                    snapshot.deleteItems([.typingPlaceholder])
                    snapshot.deleteSections([.typingIndicator])
                }
            }
            self.dataSource.apply(snapshot)
            
            if scrollTo {
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.scrollToLastRow(scrollPosition: .bottom, animated: true)
                }
            }
        }
    }
    
    func insertAttachmentCell(state: AttachmentCellState) {
        var snapshot = dataSource.snapshot()
        snapshot.appendItems([.attachment(attachmentState: state)], toSection: .main)
        DispatchQueue.main.async { [weak self] in
            self?.dataSource.apply(snapshot)
        }
    }
    
    func clearAttachmentPlaceholderCells() {
        var snapshot = dataSource.snapshot()
        snapshot.itemIdentifiers.forEach { item in
            if case .attachment(attachmentState: .uploading) = item {
                snapshot.deleteItems([item])
            }
        }
        DispatchQueue.main.async {
            self.dataSource.apply(snapshot)
        }
    }

    func presentSelectAttachmentSource() {
        let alertController = UIAlertController(
            title: .localizedText(key: .uploadFileText),
            message: .localizedText(key: .uploadFileSubtext),
            preferredStyle: .actionSheet
        )
        alertController.popoverPresentationController?.sourceView = inputTextView
        alertController.addAction(UIAlertAction(title: .localizedText(key: .photoSelectorButton), style: .default) { _ in
            self.presentPhotoPicker()
        })
        alertController.addAction(UIAlertAction(title: .localizedText(key: .fileSelectorButton), style: .default) { _ in
            self.presentFilePicker()
        })
        alertController.addAction(UIAlertAction(title: .localizedText(key: .cancelButton), style: .cancel))
        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
    
    func presentPhotoPicker() {
        var config = PHPickerConfiguration()
        config.filter = .images
        config.selectionLimit = 5
        let picker = PHPickerViewController(configuration: config)
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func presentFilePicker() {
        let supportedDocumentTypes = FileFormat.allCases.map(\.utType)
        
        let documentPicker = UIDocumentPickerViewController(
            forOpeningContentTypes: supportedDocumentTypes,
            asCopy: true
        )
        documentPicker.allowsMultipleSelection = false
        documentPicker.delegate = self
        present(documentPicker, animated: true)
    }
    
    func presentImagePreview(image: ImagePreview) {
        DispatchQueue.main.async {
            let vc = ImagePreviewViewController(image: image)
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true)
        }
    }
    
    func presentFilePreview(attachment: Attachment) {
        DispatchQueue.main.async {
            let vc = FilePreviewViewController(attachment: attachment, token: self.kindlyChatClient.token!)
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true)
        }
    }
    
    func presentAlert(title: String, message: String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController, animated: true)
        }
    }
    
    func presentChatSettings() {
        let vc = ChatSettingsViewController()
        endEditing()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func playHapticFeedback(type: UINotificationFeedbackGenerator.FeedbackType) {
        UINotificationFeedbackGenerator().notificationOccurred(.success)
    }
    
    func clearInputView() {
        inputTextView.text = ""
    }
    
    func endEditing() {
        view.endEditing(true)
    }
    
    func dismiss() {
        dismiss(animated: true)
    }
}

// MARK: Input View - Text Storage delegate

@available(iOS 14.0, *)
extension ChatViewController: NSTextStorageDelegate {
    func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if editedMask.contains(.editedCharacters) {
            inputTextView.inputPlaceholderLabel.isHidden = !inputTextView.text.isEmpty
            submitButton.isHidden = inputTextView.text.isEmpty
            
            if inputTextView.text.isEmpty {
                pinInputViewToSubmitButton.isActive = false
                pinInputViewToEdge.isActive = true
            } else {
                pinInputViewToEdge.isActive = false
                pinInputViewToSubmitButton.isActive = true
            }
        }
    }
}

// MARK: PhotosUI delegate

@available(iOS 14.0, *)
extension ChatViewController: PHPickerViewControllerDelegate {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        let itemProviders = results.map(\.itemProvider)
        for item in itemProviders where item.canLoadObject(ofClass: UIImage.self) {
            item.loadObject(ofClass: UIImage.self) { image, _ in
                guard let image = image as? UIImage else {
                    return
                }
                    
                let utType = image.cgImage?.utType
                let fileFormat = FileFormat(utType: utType as String?)

                let uploadData: Data?
                if fileFormat == .png {
                    uploadData = image.pngData()
                } else {
                    uploadData = image.jpegData(compressionQuality: 0.5)
                }

                self.presenter.didSelectImage(
                    data: uploadData,
                    suggestedName: item.suggestedName,
                    format: fileFormat
                )
            }
        }
        picker.dismiss(animated: true)
    }
}

// MARK: Document Picker delegate

@available(iOS 14.0, *)
extension ChatViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        for url in urls {
            presenter.didSelectFile(at: url)
        }
    }
}

// MARK: Data Source

@available(iOS 14.0, *)
private typealias DataSource = UITableViewDiffableDataSource<ChatSection, ChatRow>

enum ChatSection: Hashable {
    case main
    case typingIndicator
}

enum ChatRow: Hashable {
    case attachment(attachmentState: AttachmentCellState)
    case buttonGroup(buttons: [ChatButton], message: ChatMessage)
    case imageGroup(images: [ChatImage], size: ImageCarouselSize?, id: String)
    case message(ChatMessage)
    case typingPlaceholder
    case video(url: URL, id: String)
}

private enum ViewConstants {
    static let bottomMargin = 16.0
    static let inputViewVerticalMargin = 16.0
    static let submitButtonSize = 50.0
    static let toolbarSpacerMargin = 12.0
    static let verticalContentInset = 16.0
}
