import OSLog
import UIKit

enum ImagePreview {
    case chatImage(ChatImage)
    case data(Data)
}

/// Image Previewer with support for multiple formats
///
/// ImagePreviewViewController requires ``ImagePreview`` to be set.
@available(iOS 14.0, *)
final class ImagePreviewViewController: UIViewController {
    private let image: ImagePreview
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    init(image: ImagePreview) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.standardAppearance.backgroundColor = .black
        view.add(views: imageView)
        addConstraints()
        addNavigationBarButton()
        setImage()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    private func addNavigationBarButton() {
        let cancelButton = UIBarButtonItem(
            title: .localizedText(key: .cancelButton),
            image: UIImage(systemName: "xmark"),
            primaryAction: UIAction(handler: { [weak self] _ in
                self?.dismiss(animated: true)
            })
        )
        navigationItem.rightBarButtonItem = cancelButton
    }
    
    private func setImage() {
        switch image {
        case .chatImage(let chatImage):
            imageView.accessibilityLabel = chatImage.altText
            
            if chatImage.imageURL.absoluteString.hasSuffix(".gif") {
                imageView.setGifFromURL(chatImage.imageURL)
                imageView.startAnimatingGif()
            } else {
                imageView.setAsyncImage(from: chatImage.imageURL)
            }
        case .data(let data):
            imageView.image = UIImage(data: data)
        }
    }
}
