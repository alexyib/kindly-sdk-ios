import Combine
import UIKit

/// Root view states.
private enum RootView {
    /// Main chat view. Displayed when connection, config and everything is ready.
    case chat
    /// A loading view with a dedicated animation. Displayed when attempting to create a chat session.
    case loading
    /// Failed to connect to Kindly Chat API. Displayed when the device has no Internet connection.
    case retry
}

/// The main entry for presenting Kindly Chat UI.
///
/// ``KindlyChatNavigationController`` initializes `ChatViewController` (a private `UIViewController` that displays the chat).
///
/// It also configures UINavigationBar apperance and behavior.
/// - Note: Kindly SDK should be initalised before presenting KindlyChatNavigationController.
@available(iOS 14.0, *)
public final class KindlyChatNavigationController: UINavigationController {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    private var languageCode: String?
    private var cancellables = Set<AnyCancellable>()

    /// Initialize the chat UI.
    ///
    /// - Parameter languageCode: Request a language for the UI and content.
    public init(languageCode: String?) {
        self.languageCode = languageCode
        super.init(nibName: nil, bundle: nil)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ThemeManager.shared.currentTheme.background

        // MARK: Styling

        setNavigationBarStyle()

        // MARK: Behavior

        isModalInPresentation = true

        // MARK: Present correct state

        DispatchQueue.main.async {
            if self.kindlyChatClient.connectionState == .conversationEnded {
                self.kindlyChatClient.connectionState = .disconnected
            }
            self.subscribeToConnectionState()
        }

    }

    private func subscribeToConnectionState() {
        kindlyChatClient.$connectionState
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let self = self else { return }
                switch self.kindlyChatClient.connectionState {
                case .connected:
                    self.setRootView(.chat)
                case .isConnecting:
                    self.setRootView(.loading)
                case .disconnected:
                    self.setRootView(.loading)
                    self.kindlyChatClient.connect(languageCode: self.languageCode) { _ in }
                case .conversationEnded:
                    self.dismiss(animated: true)
                case .failed:
                    self.setRootView(.retry)
                }
            }
            .store(in: &cancellables)
    }

    private func setRootView(_ rootView: RootView) {
        switch rootView {
        case .chat:
            setViewControllers([ChatViewController()], animated: false)
        case .loading:
            setViewControllers([LoadingViewController()], animated: false)
        case .retry:
            setViewControllers([ErrorViewController()], animated: false)
        }
    }

    private func setNavigationBarStyle() {
        navigationBar.tintColor = ThemeManager.shared.currentTheme.navBarText
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.backgroundColor = ThemeManager.shared.currentTheme.navBarBackground
        navigationBarAppearance.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: ThemeManager.shared.currentTheme.navBarText ?? .black
        ]

        // Set same style for UINavigationBar for all appearances
        navigationBar.standardAppearance = navigationBarAppearance
        navigationBar.compactAppearance = navigationBarAppearance
        navigationBar.scrollEdgeAppearance = navigationBarAppearance
    }

    // MARK: Restart chat

    internal func restart() {
        setRootView(.loading)
    }

    // MARK: - Hidden init

    override private init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }

    override private init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override private init(navigationBarClass: AnyClass?, toolbarClass: AnyClass?) {
        super.init(navigationBarClass: navigationBarClass, toolbarClass: toolbarClass)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
