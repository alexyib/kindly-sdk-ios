import UIKit

@available(iOS 14.0, *)
final class LanguageViewController: UIViewController {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 12
        return stackView
    }()

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = .localizedText(key: .changeLanguageText)
        label.numberOfLines = 0
        label.textColor = UIColor(hex: "#6A778A")
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .subheadline)
        return label
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = .localizedText(key: .changeLanguageButton)
        view.add(views: buttonStackView)
        buttonStackView.addArrangedSubview(infoLabel)
        buttonStackView.setCustomSpacing(28, after: infoLabel)
        addConstraints()

        for language in kindlyChatClient.languages {
            addButton(language: language)
        }
    }

    private func addConstraints() {
        NSLayoutConstraint.activate([
            buttonStackView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            buttonStackView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor),
            buttonStackView.widthAnchor.constraint(equalTo: view.readableContentGuide.widthAnchor, multiplier: UIConstants.settingsButtonWidthMultiplier),
            buttonStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            buttonStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }

    private func addButton(language: Language) {
        let button = ActionButton()
        button.setTitle(language.name, for: .normal)
        button.addAction(UIAction(handler: { [weak self] _ in
            self?.kindlyChatClient.restartConversation(languageCode: language.code)
            self?.restartChat()
        }), for: .primaryActionTriggered)
        buttonStackView.addArrangedSubview(button)
    }

    // MARK: User Actions

    private func restartChat() {
        if let navigationController = navigationController as? KindlyChatNavigationController {
            navigationController.restart()
        }
    }
}
