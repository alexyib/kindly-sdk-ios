import Foundation

@available(iOS 14.0, *)
final class ChatSettingsPresenter {
    enum MenuItem {
        case changeLanguage
        case deleteChatLog
        case downloadChat
        case startOver
    }
    
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    private weak var view: ChatSettingsView?
    private let deleteChatService: DeleteChatService
    
    init(view: ChatSettingsView, deleteChatService: DeleteChatService) {
        self.view = view
        self.deleteChatService = deleteChatService
        addMenuItems()
    }
    
    // MARK: Populate the Chat settings menu

    private func addMenuItems() {
        if kindlyChatClient.languages.count > 1 {
            view?.addMenuItem(
                title: .localizedText(key: .changeLanguageButton),
                icon: "icon_language",
                type: .changeLanguage
            )
        }
        view?.addMenuItem(
            title: .localizedText(key: .startOverButton),
            icon: "icon_restart",
            type: .startOver
        )
        view?.addMenuItem(
            title: .localizedText(key: .deleteButton),
            icon: "icon_trash",
            type: .deleteChatLog
        )
        view?.addMenuItem(
            title: .localizedText(key: .downloadButton),
            icon: "icon_download",
            type: .downloadChat
        )
    }

    // MARK: View Actions
    
    func didPressMenuItem(_ menuItem: MenuItem) {
        switch menuItem {
        case .changeLanguage:
            view?.presentChangeLanguage()
        case .startOver:
            kindlyChatClient.restartConversation()
            view?.restartChat()
        case .deleteChatLog:
            view?.presentDeleteChatConfirmation()
        case .downloadChat:
            view?.presentDownloadConversationConfirmation()
        }
    }
    
    func didPressDownloadPreview(format: DownloadFormat) {
        view?.presentDownloadChatView(format: format)
    }
    
    func didConfirmDeleteChat() {
        guard let token = kindlyChatClient.token else {
            return
        }
        deleteChatService.deleteChatLog(token: token)
        kindlyChatClient.endActiveChat()
    }
}
