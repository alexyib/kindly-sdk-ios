import UIKit

@available(iOS 14.0, *)
protocol ChatSettingsView: AnyObject {
    func addMenuItem(title: String, icon: String, type: ChatSettingsPresenter.MenuItem)
    func presentChangeLanguage()
    func presentDeleteChatConfirmation()
    func presentDownloadConversationConfirmation()
    func presentDownloadChatView(format: DownloadFormat)
    func restartChat()
    func dismiss()
}

/// Settings view for a Kindly conversation.
@available(iOS 14.0, *)
final class ChatSettingsViewController: UIViewController {
    private var presenter: ChatSettingsPresenter!
    
    // MARK: Views
    
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = ViewConstants.buttonSpacing
        return stackView
    }()
    
    private lazy var poweredByLabel: UILabel = {
        let label = UILabel()
        label.attributedText = poweredByText()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = UIColor(hex: "#6A778A")
        return label
    }()

    // MARK: View lifecycle and config
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ChatSettingsPresenter(
            view: self,
            deleteChatService: DeleteChatService(httpClient: HTTPClient())
        )
        title = .localizedText(key: .settingsHeader)
        view.backgroundColor = .white
        navigationItem.backButtonDisplayMode = .minimal
        view.add(views: buttonStackView, poweredByLabel)
        addConstraints()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            buttonStackView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor, constant: ViewConstants.stackViewVerticalMargin),
            buttonStackView.bottomAnchor.constraint(lessThanOrEqualTo: poweredByLabel.topAnchor, constant: ViewConstants.stackViewVerticalMargin),
            buttonStackView.widthAnchor.constraint(equalTo: view.readableContentGuide.widthAnchor, multiplier: UIConstants.settingsButtonWidthMultiplier),
            buttonStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            buttonStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            poweredByLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -ViewConstants.bottomMargin),
            poweredByLabel.widthAnchor.constraint(equalTo: view.widthAnchor),
            poweredByLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
    
    private func poweredByText() -> NSAttributedString {
        let content = NSMutableAttributedString(string: "Powered by ")
        let logoAttachment = NSTextAttachment()
        logoAttachment.image = UIImage(named: "icon_kindly", in: .module, with: nil)
        logoAttachment.adjustsImageSizeForAccessibilityContentSizeCategory = true
        content.append(NSAttributedString(attachment: logoAttachment))
        return content
    }
}

// MARK: ChatSettingsView implementation

@available(iOS 14.0, *)
extension ChatSettingsViewController: ChatSettingsView {
    func addMenuItem(title: String, icon: String, type: ChatSettingsPresenter.MenuItem) {
        let button = ActionButton()
        button.setTitle(title, for: .normal)
        button.setImage(UIImage(named: icon, in: .module, with: nil), for: .normal)
        button.addAction(UIAction(handler: { [weak self] _ in
            self?.presenter.didPressMenuItem(type)
        }), for: .primaryActionTriggered)
        buttonStackView.addArrangedSubview(button)
    }

    func presentChangeLanguage() {
        let vc = LanguageViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentDeleteChatConfirmation() {
        let alertController = UIAlertController(
            title: .localizedText(key: .deleteText),
            message: nil,
            preferredStyle: .actionSheet
        )
        
        alertController.addAction(UIAlertAction(
            title: .localizedText(key: .deleteButton),
            style: .destructive
        ) { [weak self] _ in
            self?.presenter.didConfirmDeleteChat()
        })
        alertController.addAction(UIAlertAction(title: .localizedText(key: .cancelButton), style: .cancel))
        alertController.popoverPresentationController?.sourceView = buttonStackView
        present(alertController, animated: true)
    }
    
    func presentDownloadConversationConfirmation() {
        let alertController = UIAlertController(
            title: .localizedText(key: .downloadButton),
            message: .localizedText(key: .downloadFormatText),
            preferredStyle: .actionSheet
        )
        alertController.addAction(UIAlertAction(title: "HTML", style: .default) { [weak self] _ in
            self?.presenter.didPressDownloadPreview(format: .html)
        })
        alertController.addAction(UIAlertAction(title: "JSON", style: .default) { [weak self] _ in
            self?.presenter.didPressDownloadPreview(format: .json)
        })
        
        alertController.addAction(UIAlertAction(title: .localizedText(key: .cancelButton), style: .cancel))
        alertController.popoverPresentationController?.sourceView = buttonStackView
        present(alertController, animated: true)
    }
    
    func presentDownloadChatView(format: DownloadFormat) {
        let vc = DownloadChatViewController(format: format)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func restartChat() {
        if let navigationController = navigationController as? KindlyChatNavigationController {
            navigationController.restart()
        }
    }
    
    func dismiss() {
        dismiss(animated: true)
    }
}

private enum ViewConstants {
    static let bottomMargin = 16.0
    static let buttonSpacing = 10.0
    static let stackViewVerticalMargin = 8.0
}
