import UIKit

/// Displayed when the SDK fails to create a chat session.
@available(iOS 14.0, *)
final class ErrorViewController: UIViewController {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Failed to start chat."
        label.numberOfLines = 0
        label.textColor = UIColor(hex: "#6A778A")
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    private lazy var retryButton: ActionButton = {
        let button = ActionButton()
        button.setTitle("Try again", for: .normal)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ThemeManager.shared.currentTheme.background
        view.add(views: titleLabel, retryButton)
        addConstraints()
        addActions()
        addNavigationBarButton()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 120),
            titleLabel.widthAnchor.constraint(equalTo: view.readableContentGuide.widthAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            retryButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 24),
            retryButton.leadingAnchor.constraint(equalTo: view.readableContentGuide.leadingAnchor, constant: 16),
            retryButton.trailingAnchor.constraint(equalTo: view.readableContentGuide.trailingAnchor, constant: -16),
            retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            retryButton.heightAnchor.constraint(equalToConstant: 52),
        ])
    }
    
    private func addActions() {
        retryButton.addAction(UIAction(handler: { [weak self] _ in
            self?.didPressReconnectButton()
        }), for: .primaryActionTriggered)
    }
    
    private func addNavigationBarButton() {
        let closeButton = UIBarButtonItem(
            title: "Close",
            image: UIImage(systemName: "chevron.down"),
            primaryAction: UIAction(handler: { [weak self] _ in
                self?.dismiss(animated: true)
            })
        )
        navigationItem.rightBarButtonItem = closeButton
    }
    
    // MARK: User Actions
    
    private func didPressReconnectButton() {
        kindlyChatClient.loadConfigFromKindly()
        kindlyChatClient.connect() { _ in }
    }
}
