import UIKit
import WebKit

/// Preview documents of type ``Attachment``.
@available(iOS 14.0, *)
final class FilePreviewViewController: UIViewController {
    private let attachment: Attachment
    private let token: String
    private let attachmentService = AttachmentService(httpClient: HTTPClient())
    private let webView = WKWebView()

    init(attachment: Attachment, token: String) {
        self.attachment = attachment
        self.token = token
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = attachment.name
        view.backgroundColor = .white
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.standardAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationController?.navigationBar.standardAppearance.backgroundColor = .white
        webView.backgroundColor = .clear
        webView.allowsLinkPreview = false
        view.add(views: webView)
        addConstraints()
        addBarButtonItems()
        downloadAndDisplayAttachment()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    private func addBarButtonItems() {
        let cancelButton = UIBarButtonItem(
            title: .localizedText(key: .cancelButton),
            image: UIImage(systemName: "xmark"),
            primaryAction: UIAction(handler: { [weak self] _ in
                self?.dismiss(animated: true)
            })
        )
        navigationItem.rightBarButtonItem = cancelButton
    }

    private func downloadAndDisplayAttachment() {
        Task {
            guard let data = try await attachmentService.download(
                storagePath: attachment.storagePath,
                token: token
            ) else {
                dismiss(animated: true)
                return
            }
            
            _ = webView.load(
                data,
                mimeType: attachment.format.mimeType,
                characterEncodingName: "utf-8",
                baseURL: API.url(for: .attachment(storagePath: attachment.storagePath))
            )
        }
    }
}
