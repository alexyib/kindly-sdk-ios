import UIKit

@available(iOS 14.0, *)
final class LoadingViewController: UIViewController {
    private lazy var bubbleAnimationView = BubbleAnimationView(
        color: .black, bubbleSize: 12, bounceDistance: 10
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ThemeManager.shared.currentTheme.background
        view.add(views: bubbleAnimationView)
        addConstraints()
        addNavigationBarButton()
        DispatchQueue.main.async {
            self.bubbleAnimationView.startAnimating()
        }
    }

    private func addConstraints() {
        NSLayoutConstraint.activate([
            bubbleAnimationView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            bubbleAnimationView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    private func addNavigationBarButton() {
        let closeButton = UIBarButtonItem(
            title: "Close",
            image: UIImage(systemName: "chevron.down"),
            primaryAction: UIAction(handler: { [weak self] _ in
                self?.dismiss(animated: true)
            })
        )
        navigationItem.rightBarButtonItem = closeButton
    }
}
