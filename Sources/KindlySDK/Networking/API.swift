import Foundation

@available(iOS 14.0, *)
internal struct API {
    @Injected<KindlyChatClient> private static var kindlyChatClient: KindlyChatClient

    private static var rootURL: URL {
        switch kindlyChatClient.config.environment {
        case .production:
            return URL(string: "https://bot.kindly.ai/chatbubble/v2")!
        case .dev:
            return URL(string: "https://bot-dev.kindly.ai/chatbubble/v2")!
        }
    }

    /// Generate a complete URL from `Endpoint`.
    static func url(for endpoint: Endpoint) -> URL {
        return API.rootURL.appendingPathComponent(endpoint.path)
    }
}
