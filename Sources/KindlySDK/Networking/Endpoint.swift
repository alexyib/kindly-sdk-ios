import Foundation

internal struct Endpoint {
    var path: String
    var method: HTTPMethod
}

// MARK: Available API endpoints

extension Endpoint {
    static func attachment(storagePath: String) -> Self {
        Endpoint(path: "attachment/\(storagePath)", method: .GET)
    }
    
    static var attachmentCreate: Self {
        Endpoint(path: "attachment/create", method: .POST)
    }
    
    static var authPusher: Self {
        Endpoint(path: "auth/pusher", method: .POST)
    }
    
    static var buttonClick: Self {
        Endpoint(path: "button/click", method: .POST)
    }
    
    static var connect: Self {
        Endpoint(path: "connect", method: .POST)
    }
    
    static var deleteChatLog: Self {
        Endpoint(path: "privacy/delete", method: .DELETE)
    }
    
    static var exportHTML: Self {
        Endpoint(path: "privacy/export/html", method: .GET)
    }
    
    static var exportJSON: Self {
        Endpoint(path: "privacy/export/json", method: .GET)
    }
    
    static var message: Self {
        Endpoint(path: "message", method: .POST)
    }
    
    static func settingsURL(for botKey: String) -> URL {
        URL(string: "https://chat.kindlycdn.com/settings/\(botKey).json")!
    }
    
    static var uploadsComplete: Self {
        Endpoint(path: "attachment/uploads-complete", method: .POST)
    }
    
    static var userLeft: Self {
        Endpoint(path: "user_left", method: .PATCH)
    }
    
    static var chatAuth: Self {
        Endpoint(path: "auth/chat", method: .POST)
    }
}
