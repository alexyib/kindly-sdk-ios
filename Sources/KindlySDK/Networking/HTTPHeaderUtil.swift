import Foundation
import UIKit.UIDevice

@available(iOS 14.0, *)
struct HTTPHeaderUtil {
    
    @Injected<KindlyChatClient> static private var kindlyChatClient: KindlyChatClient
    
    static var defaultHeaders: [HTTPHeader] {
        let headers: [HTTPHeader] = [
            .kindlyChatClient(value: "ios"),
            .kindlyChatIdentifier(value: deviceIdentifier),
            .kindlyChatVersion(value: formattedSDKVersion),
        ]
        
        return headers
    }

    private static var deviceIdentifier: String {
        return UIDevice.current.identifierForVendor?.uuidString ?? UUID().uuidString
    }
    
    private static var formattedSDKVersion: String {
        let currentVersion = "2.0.0"
        return "v\(currentVersion)-sdk"
    }
}
