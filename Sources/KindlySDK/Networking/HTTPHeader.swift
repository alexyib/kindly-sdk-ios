internal struct HTTPHeader {
    let field: String
    let value: String
}

extension HTTPHeader {
    static func accept(value: String) -> Self {
        HTTPHeader(field: "Accept", value: value)
    }
    
    static func contentLength(value: String) -> Self {
        HTTPHeader(field: "Content-Length", value: value)
    }
    
    static func contentType(value: String) -> Self {
        HTTPHeader(field: "Content-Type", value: value)
    }
    
    static func kindlyBotKey(value: String) -> Self {
        HTTPHeader(field: "kindly-bot-key", value: value)
    }
    
    static func kindlyChatBubbleToken(value: String) -> Self {
        HTTPHeader(field: "kindly-chatbubble-token", value: value)
    }
    
    static func kindlyChatClient(value: String) -> Self {
        HTTPHeader(field: "kindly-chat-client", value: value)
    }
    
    static func kindlyChatVersion(value: String) -> Self {
        HTTPHeader(field: "kindly-chat-version", value: value)
    }
    
    static func kindlyChatIdentifier(value: String) -> Self {
        HTTPHeader(field: "kindly-chat-uuid", value: value)
    }
    
    static func kindlyUserAuthorization(value: String) -> Self {
        HTTPHeader(field: "Authorization", value: "Bearer \(value)")
    }
}
