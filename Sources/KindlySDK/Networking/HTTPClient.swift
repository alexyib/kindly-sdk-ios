import Foundation
import OSLog

@available(iOS 14.0, *)
internal protocol HTTPClientProtocol {
    func request(
        from url: URL,
        method: HTTPMethod,
        headers: [HTTPHeader],
        queryItems: [URLQueryItem],
        body: Data?,
        shouldCacheResponse: Bool
    ) async throws -> Data?

    func download(from url: URL, headers: [HTTPHeader], saveToURL: URL) async throws -> URL
}

/// HTTP Client for the Kindly SDK.
@available(iOS 14.0, *)
internal struct HTTPClient: HTTPClientProtocol {
    
    /// Perform a HTTP Request.
    @discardableResult
    func request(from url: URL, method: HTTPMethod, headers: [HTTPHeader], queryItems: [URLQueryItem], body: Data?, shouldCacheResponse: Bool) async throws -> Data? {
        guard let requestURL = createRequestURL(baseURL: url, queryItems: queryItems) else {
            throw KindlySDKError.invalidURL
        }

        Logger.shared.log(level: .debug, "🌍 \(method.rawValue): \(requestURL)")

        var request = URLRequest(url: requestURL)
        request.httpMethod = method.rawValue
        request.httpBody = body
        request.cachePolicy = .reloadIgnoringLocalCacheData

        // Add specified HTTP headers.
        headers.forEach { header in
            request.setValue(header.value, forHTTPHeaderField: header.field)
        }
        // Add default HTTP headers that should be included in every request.
        HTTPHeaderUtil.defaultHeaders.forEach { header in
            request.setValue(header.value, forHTTPHeaderField: header.field)
        }
        // auth token
        @Injected<KindlyChatClient> var kindlyChatClient: KindlyChatClient
        if let jwtToken = KeychainService.clientJWTToken.read(),
           kindlyChatClient.connectionState == .connected,
           !jwtToken.string.isEmpty,
           KeychainService.clientJWTToken.isExpired == true,
           kindlyChatClient.authenticationState == .authenticated {
            try? await kindlyChatClient.authenticateWithAuthToken(forceNew: true)
        }

        // Return cached data if saved to URLCache.
        if shouldCacheResponse, let cachedResponse = URLCache.shared.cachedResponse(for: request) {
            return cachedResponse.data
        }

        let (data, response) = try await URLSession.shared.data(from: request)
        if let response = response as? HTTPURLResponse {
            if case 400 ... 599 = response.statusCode {
                Logger.shared.log(level: .debug, "🔴 Status Code: \(response.statusCode) for \(requestURL)")
                handleHTTPError(response: response)
                throw KindlySDKError.httpError(statusCode: response.statusCode)
            }
        }

        if let data = data, shouldCacheResponse {
            URLCache.shared.storeCachedResponse(.init(response: response, data: data), for: request)
        }

        return data
    }

    /// Perform a HTTP download request.
    ///
    /// Specify a file path where the downloaded file should be stored.
    @discardableResult
    func download(from url: URL, headers: [HTTPHeader], saveToURL: URL) async throws -> URL {
        Logger.shared.log(level: .debug, "⬇️ Downloading from \(url)")

        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.GET.rawValue
        request.cachePolicy = .reloadIgnoringLocalCacheData

        // Add specified HTTP headers.
        headers.forEach { header in
            request.setValue(header.value, forHTTPHeaderField: header.field)
        }
        // Add default HTTP headers that should be included in every request.
        HTTPHeaderUtil.defaultHeaders.forEach { header in
            request.setValue(header.value, forHTTPHeaderField: header.field)
        }
        
        return try await URLSession.shared.download(request: request, saveToURL: saveToURL)
    }

    // MARK: Helper functions

    private func createRequestURL(baseURL: URL, queryItems: [URLQueryItem]) -> URL? {
        if queryItems.isEmpty {
            return baseURL
        }

        var requestURL = URLComponents(url: baseURL, resolvingAgainstBaseURL: true)
        requestURL?.queryItems = queryItems
        return requestURL?.url
    }
    
    private func handleHTTPError(response: HTTPURLResponse) {
        if response.statusCode == 401 {
            @Injected<KindlyChatClient> var kindlyChatClient: KindlyChatClient
            if kindlyChatClient.authenticationState == .authenticated {
                _ = kindlyChatClient.refreshAuthToken()
            }
        }
    }
}

// MARK: Extensions

@available(iOS 14.0, *)
internal extension HTTPClientProtocol {
    /// Extension to support default parameter arguments.
    @discardableResult
    func request(from url: URL, method: HTTPMethod, headers: [HTTPHeader] = [], queryItems: [URLQueryItem] = [], body: Data? = nil, shouldCacheResponse: Bool = false) async throws -> Data? {
        try await request(from: url, method: method, headers: headers, queryItems: queryItems, body: body, shouldCacheResponse: shouldCacheResponse)
    }

    /// Extension for conveniently using `Endpoint` links instead of URL.
    @discardableResult
    func request(endpoint: Endpoint, headers: [HTTPHeader] = [], queryItems: [URLQueryItem] = [], body: Data? = nil, shouldCacheResponse: Bool = false) async throws -> Data? {
        return try await request(from: API.url(for: endpoint), method: endpoint.method, headers: headers, queryItems: queryItems, body: body, shouldCacheResponse: shouldCacheResponse)
    }
}
