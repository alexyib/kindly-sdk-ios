import UIKit

@available(iOS 14.0, *)
final class NoInternetView: UIView {
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icon_no_connection", in: .module, with: nil)
        return imageView
    }()

    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = "You seem to be offline, please check your internet connection"
        label.textColor = .black.withAlphaComponent(0.7)
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .subheadline)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        backgroundColor = UIColor(hex: "#F9EF94")
        add(views: iconView, infoLabel)
        addConstraints()
    }

    private func addConstraints() {
        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: ViewConstants.verticalInset),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewConstants.horizontalInset),
            iconView.widthAnchor.constraint(equalToConstant: ViewConstants.iconSize),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),

            infoLabel.topAnchor.constraint(equalTo: iconView.topAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -ViewConstants.verticalInset),
            infoLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: ViewConstants.spacing),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -ViewConstants.horizontalInset),
        ])
    }
}

private enum ViewConstants {
    static let horizontalInset = 16.0
    static let iconSize = 20.0
    static let spacing = 10.0
    static let verticalInset = 10.0
}
