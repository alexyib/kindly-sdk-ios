import OSLog
import UIKit

private enum ImageCellStyle {
    /// Displays an image without text.
    case image
    /// Displays an image with title and/or subtitle.
    case product
}

/// Image View for displaying a single ``ChatImage``.
///
/// ImageCell provides labels for Title and Description. If any of these are provided, they will be displayed below the image.
/// Can be styled with ``ImageCellStyle``.
@available(iOS 14.0, *)
final class ImageCell: UICollectionViewCell {
    weak var delegate: ChatInteractionDelegate?

    private var style = ImageCellStyle.image
    private var size: ImageCarouselSize?

    // MARK: UI Components
    
    /// Extra container view for adding a nice shadow.
    private var containerView = UIView()

    /// Vertical stack for displaying title and subtitle labels.
    private var textStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = ThemeManager.shared.currentTheme.botMessageBackground
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = ThemeManager.shared.currentTheme.botMessageText
        label.numberOfLines = 0
        label.isUserInteractionEnabled = false
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        label.isHidden = true
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor(hex: "#6A778A")
        label.numberOfLines = 0
        label.isUserInteractionEnabled = false
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        label.isHidden = true
        return label
    }()
    
    private var linkButton: UIButton = {
        let button = UIButton()
        if #available(iOS 15.0, *) {
            button.setBackgroundImage(UIImage(named: "icon_filled_link", in: .module, with: nil), for: .normal)
        } else {
            button.setBackgroundImage(UIImage(systemName: "link.circle.fill"), for: .normal)
            button.tintColor = .white
        }
        button.accessibilityLabel = "Open link"
        button.isHidden = true
        return button
    }()

    private var fullScreenButton: UIButton = {
        let button = UIButton()
        if #available(iOS 15.0, *) {
            button.setBackgroundImage(UIImage(named: "icon_filled_full_screen", in: .module, with: nil), for: .normal)
        } else {
            button.setBackgroundImage(UIImage(systemName: "arrow.up.forward.app.fill"), for: .normal)
            button.tintColor = .white
        }
        button.accessibilityLabel = "Open full screen preview"
        button.isUserInteractionEnabled = true
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View lifecycle and configuration
    
    private func setup() {
        contentView.add(views: containerView)
        containerView.add(views: imageView, textStackView, linkButton, fullScreenButton)
        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(descriptionLabel)
        addConstraints()

        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = ViewConstants.cornerRadius
        containerView.clipsToBounds = true
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = ThemeManager.shared.currentTheme.buttonOutline?.cgColor
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            
            imageView.topAnchor.constraint(equalTo: containerView.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            imageView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            
            textStackView.topAnchor.constraint(equalTo: imageView.bottomAnchor),
            textStackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            textStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            textStackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            
            // Overlay buttons
            
            linkButton.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
            linkButton.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            linkButton.widthAnchor.constraint(equalToConstant: ViewConstants.buttonSize),
            linkButton.heightAnchor.constraint(equalTo: linkButton.widthAnchor),
            
            fullScreenButton.topAnchor.constraint(equalTo: imageView.topAnchor),
            fullScreenButton.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            fullScreenButton.widthAnchor.constraint(equalToConstant: ViewConstants.buttonSize),
            fullScreenButton.heightAnchor.constraint(equalTo: fullScreenButton.widthAnchor),
        ])
    }
    
    // MARK: Styling

    private func addShadow() {
        contentView.layer.shadowColor = ThemeManager.shared.currentTheme.defaultShadow?.cgColor
        contentView.layer.shadowOffset = CGSize(width: 0, height: 2)
        contentView.layer.shadowRadius = 6
        contentView.layer.shadowOpacity = 0.08
    }
    
    // MARK: View events

    override func layoutSubviews() {
        super.layoutSubviews()
        switch style {
        case .image:
            contentView.layer.shadowPath = UIBezierPath(
                roundedRect: contentView.bounds,
                cornerRadius: ViewConstants.cornerRadius
            ).cgPath
        case .product:
            contentView.layer.shadowPath = nil
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        updateSize(size ?? .medium)
        // Clear previously added actions
        fullScreenButton.removeEventHandlers()
        linkButton.removeEventHandlers()
    }
    
    override var isHighlighted: Bool {
        didSet {
            alpha = isHighlighted ? UIConstants.highlightAlpha : 1
        }
    }

    // MARK: Update content
    
    func update(chatImage: ChatImage, size: ImageCarouselSize) {
        self.size = size
        updateSize(size)
        
        // MARK: Set image
        
        if chatImage.imageURL.absoluteString.hasSuffix(".gif") {
            imageView.setGifFromURL(chatImage.imageURL)
            imageView.startAnimatingGif()
        } else {
            imageView.setAsyncImage(from: chatImage.imageURL)
        }
        
        imageView.accessibilityLabel = chatImage.altText
        
        // MARK: Styling
        
        if chatImage.title?.count == 0, chatImage.description?.count == 0 {
            style = .image
        } else {
            style = .product
        }
        
        switch style {
        case .image:
            contentView.layer.shadowPath = nil
            textStackView.directionalLayoutMargins = .zero
        case .product:
            addShadow()
            textStackView.directionalLayoutMargins = .init(
                top: ViewConstants.textContentVerticalInset,
                leading: ViewConstants.textContentHorizontalInset,
                bottom: ViewConstants.textContentVerticalInset,
                trailing: ViewConstants.textContentHorizontalInset
            )
        }
        
        // MARK: Set title and subtitle
        
        if let title = chatImage.title {
            titleLabel.text = title
            titleLabel.isHidden = false
        } else {
            titleLabel.isHidden = true
        }
        
        if let description = chatImage.description {
            descriptionLabel.text = description
            descriptionLabel.isHidden = false
        } else {
            descriptionLabel.isHidden = true
        }
        
        // MARK: Add full screen button
        
        fullScreenButton.addAction(UIAction(handler: { [weak self] _ in
            self?.delegate?.didPressImagePreview(type: .chatImage(chatImage))
        }), for: .primaryActionTriggered)
        
        // MARK: Add link button
        
        if let linkURL = chatImage.linkURL {
            linkButton.isHidden = false
            linkButton.addAction(UIAction(handler: { [weak self] _ in
                self?.delegate?.didPressLink(url: linkURL)
            }), for: .primaryActionTriggered)
        } else {
            linkButton.isHidden = true
        }
    }
    
    private func updateSize(_ size: ImageCarouselSize) {
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: size.imageSize),
            imageView.widthAnchor.constraint(equalToConstant: size.imageSize),
        ])
    }
}

private enum ViewConstants {
    static let buttonSize = 50.0
    static let cornerRadius = 8.0
    static let textContentHorizontalInset = 10.0
    static let textContentVerticalInset = 12.0
}
