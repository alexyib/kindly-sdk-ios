import UIKit

/// Displays a preview of the linked video.
@available(iOS 14.0, *)
final class VideoCell: UITableViewCell {
    weak var delegate: ChatInteractionDelegate?
    private var url: URL?
    
    private lazy var chatBubbleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIConstants.messageCornerRadius
        view.backgroundColor = UIColor(hex: "#2C2C2C")
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .caption2)
        label.textColor = .white
        return label
    }()
    
    private lazy var playIconView: UIImageView = {
        let imageView = UIImageView(image: UIImage(systemName: "play.circle.fill"))
        imageView.contentMode = .scaleAspectFit
        imageView.accessibilityLabel = "Play Video"
        imageView.tintColor = .white
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        selectionStyle = .none
        backgroundColor = .clear
        
        contentView.add(views: chatBubbleView)
        chatBubbleView.add(views: playIconView, titleLabel)
        addConstraints()
        addGestureRecognizer()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            chatBubbleView.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatBubbleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -UIConstants.messageCellSpacing),
            chatBubbleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: UIConstants.horizontalMargin),
            chatBubbleView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -UIConstants.horizontalMargin),
            chatBubbleView.widthAnchor.constraint(lessThanOrEqualTo: contentView.widthAnchor, multiplier: UIConstants.messageMaxWidthMultiplier),
            
            playIconView.topAnchor.constraint(equalTo: chatBubbleView.topAnchor, constant: 32),
            playIconView.widthAnchor.constraint(equalToConstant: 54),
            playIconView.heightAnchor.constraint(equalTo: playIconView.widthAnchor),
            playIconView.centerXAnchor.constraint(equalTo: chatBubbleView.centerXAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: playIconView.bottomAnchor, constant: 12),
            titleLabel.bottomAnchor.constraint(equalTo: chatBubbleView.bottomAnchor, constant: -32),
            titleLabel.leadingAnchor.constraint(equalTo: chatBubbleView.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: chatBubbleView.trailingAnchor, constant: -16),
        ])
    }
    
    private func addGestureRecognizer() {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(didPressVideoLink)
        )
        chatBubbleView.addGestureRecognizer(tapGesture)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: true)
        alpha = highlighted ? UIConstants.highlightAlpha : 1
    }

    // MARK: - Update content

    func update(url: URL) {
        self.url = url
        titleLabel.text = url.absoluteString
    }

    // MARK: User Actions
    
    @objc private func didPressVideoLink() {
        if let url = url {
            delegate?.didPressLink(url: url)
        }
    }
}
