import SwiftyGif
import UIKit

/// Image viewer for embeddeding in the chat view.
///
/// Supports displaying one or multiple images.
@available(iOS 14.0, *)
final class ImageCarouselCell: UITableViewCell {
    weak var delegate: ChatInteractionDelegate?

    private var images = [ChatImage]()
    private var size: ImageCarouselSize?

    private var carouselView: SelfSizingCollectionView!

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        selectionStyle = .none
        backgroundColor = .clear

        carouselView = .init(frame: .zero, collectionViewLayout: collectionViewLayout)
        contentView.add(views: carouselView)
        setupCollectionView()
        addConstraints()
    }

    private func setupCollectionView() {
        carouselView.dataSource = self
        carouselView.delegate = self
        carouselView.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.identifier)
        carouselView.backgroundColor = .clear
        carouselView.showsHorizontalScrollIndicator = false
    }

    private func addConstraints() {
        NSLayoutConstraint.activate([
            carouselView.topAnchor.constraint(equalTo: contentView.topAnchor),
            carouselView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -UIConstants.messageCellSpacing),
            carouselView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            carouselView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
        ])
    }

    private var collectionViewLayout: UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.sectionInset.left = UIConstants.horizontalMargin
        layout.sectionInset.right = UIConstants.horizontalMargin
        return layout
    }

    // MARK: - Update content

    func update(images: [ChatImage], size: ImageCarouselSize?) {
        // Update content and size
        self.images = images
        self.size = size ?? .medium

        // Update UICollectionView and make sure the layout adapts correct size.
        carouselView.reloadData()
    }
}

// MARK: Delegates

@available(iOS 14.0, *)
extension ImageCarouselCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.identifier, for: indexPath) as! ImageCell
        let chatImage = images[indexPath.item]
        cell.update(chatImage: chatImage, size: size ?? .medium)
        cell.delegate = self
        return cell
    }
}

@available(iOS 14.0, *)
extension ImageCarouselCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let chatImage = images[indexPath.item]
        if let linkURL = chatImage.linkURL {
            delegate?.didPressLink(url: linkURL)
        } else {
            delegate?.didPressImagePreview(type: .chatImage(chatImage))
        }
    }
}

@available(iOS 14.0, *)
extension ImageCarouselCell: ChatInteractionDelegate {
    func didPressImagePreview(type: ImagePreview) {
        delegate?.didPressImagePreview(type: type)
    }

    func didPressLink(url: URL) {
        delegate?.didPressLink(url: url)
    }

    func didPressAttachmentCell(attachment: AttachmentCellState?) {}
    func didPressButton(chatButton: ChatButton, chatMessage: ChatMessage) {}
    func didPressImagePreview(imageData: Data) {}
}
