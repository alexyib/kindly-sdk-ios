import UIKit

/// A cell representing that the bot or agent is typing.
final class TypingIndicatorCell: UITableViewCell {
    private lazy var chatBubbleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = UIConstants.messageCornerRadius
        view.backgroundColor = ThemeManager.shared.currentTheme.botMessageBackground
        return view
    }()
    
    private var bubbleAnimationView = BubbleAnimationView()
    
    // MARK: Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        alpha = 0
        selectionStyle = .none
        backgroundColor = .clear
        contentView.add(views: chatBubbleView)
        chatBubbleView.add(views: bubbleAnimationView)
        addConstraints()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            chatBubbleView.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatBubbleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -UIConstants.messageCellSpacing),
            chatBubbleView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: UIConstants.horizontalMargin),
            chatBubbleView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -UIConstants.horizontalMargin),
            chatBubbleView.widthAnchor.constraint(lessThanOrEqualTo: contentView.widthAnchor, multiplier: UIConstants.messageMaxWidthMultiplier),
            
            bubbleAnimationView.topAnchor.constraint(equalTo: chatBubbleView.topAnchor, constant: 12),
            bubbleAnimationView.bottomAnchor.constraint(equalTo: chatBubbleView.bottomAnchor, constant: -12),
            bubbleAnimationView.leadingAnchor.constraint(equalTo: chatBubbleView.leadingAnchor, constant: 16),
            bubbleAnimationView.trailingAnchor.constraint(equalTo: chatBubbleView.trailingAnchor, constant: -16)
        ])
    }
}
