import UIKit

/// Displays a stack view with interactive buttons in the chat view.
@available(iOS 14.0, *)
final class ButtonGroupCell: UITableViewCell {
    weak var delegate: ChatInteractionDelegate?
    private var message: ChatMessage?

    private var buttonStackView = UIStackView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        selectionStyle = .none
        backgroundColor = .clear
        contentView.add(views: buttonStackView)
        addConstraints()
        
        buttonStackView.axis = .vertical
        buttonStackView.spacing = 8
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            buttonStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            buttonStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -UIConstants.messageCellSpacing),
            buttonStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: UIConstants.horizontalMargin),
            buttonStackView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -UIConstants.horizontalMargin),
            buttonStackView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: UIConstants.messageMaxWidthMultiplier),
        ])
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        buttonStackView.removeSubviews()
    }
    
    // MARK: - Update content

    func update(buttons: [ChatButton], message: ChatMessage) {
        self.message = message
        
        buttons.forEach { chatButton in
            let button = createActionButton(for: chatButton)
            buttonStackView.addArrangedSubview(button)
        }
    }
    
    private func createActionButton(for chatButton: ChatButton) -> ActionButton {
        let button = ActionButton()
        if chatButton.type == .link {
            let linkIcon = UIImage(named: "icon_link", in: .module, with: nil)!
            button.setTitleLabel(text: chatButton.label, icon: linkIcon)
        } else {
            button.setTitle(chatButton.label, for: .normal)
        }
        
        if chatButton.hasBeenSelected {
            button.setSelected()
        }
        
        button.addAction(UIAction(handler: { [weak self] _ in
            if let message = self?.message {
                self?.delegate?.didPressButton(chatButton: chatButton, chatMessage: message)
                button.setSelected()
                UIImpactFeedbackGenerator(style: .light).impactOccurred()
                button.isEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    button.isEnabled = true
                }
            }
        }), for: .primaryActionTriggered)
        return button
    }
}
