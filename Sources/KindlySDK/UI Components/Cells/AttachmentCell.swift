import UIKit

enum AttachmentCellState: Hashable {
    case content(attachment: Attachment)
    case uploading(previewImage: UIImage? = nil)
    case uploadFailed(retryRequest: AttachmentRequest)
}

/// Displays a preview of an attachment.
/// AttachmentCell supports multiple states from uploading to presentation.
@available(iOS 14.0, *)
final class AttachmentCell: UITableViewCell {
    weak var delegate: ChatInteractionDelegate?
    
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient
    
    // MARK: UI Components
    
    private lazy var chatBubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = ThemeManager.shared.currentTheme.botMessageBackground
        view.layer.borderColor = ThemeManager.shared.currentTheme.buttonOutline?.cgColor
        view.layer.cornerRadius = ViewConstants.cornerRadius
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 2, leading: 0, bottom: 2, trailing: 0)
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .gray
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        label.isHidden = true
        return label
    }()
    
    private lazy var contentImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    private lazy var fullScreenButton: UIButton = {
        let button = UIButton()
        button.setBackgroundImage(UIImage(named: "icon_filled_full_screen", in: .module, with: nil), for: .normal)
        button.isHidden = true
        button.accessibilityLabel = "Open full screen preview"
        return button
    }()
    
    private lazy var blurView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: .systemMaterialDark)
        let view = UIVisualEffectView(effect: effect)
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var bubbleAnimationView = BubbleAnimationView(
        color: .black,
        bubbleSize: 12,
        bounceDistance: 10
    )
    
    // MARK: State
    
    var state: AttachmentCellState? {
        didSet {
            switch state {
            case .content(let attachment):
                setContentState(attachment: attachment)
            case .uploading(let previewImage):
                setUploadingState(previewImage: previewImage)
            case .uploadFailed:
                setUploadFailedState()
            case .none:
                break
            }
        }
    }
    
    // MARK: View lifecycle and configuration
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        selectionStyle = .none
        backgroundColor = .clear
        contentView.add(views: chatBubbleView)
        chatBubbleView.add(views: contentImageView, blurView, contentStackView, fullScreenButton)
        contentStackView.addArrangedSubview(bubbleAnimationView)
        contentStackView.addArrangedSubview(titleLabel)
        addConstraints()
        addTapGesture()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            chatBubbleView.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatBubbleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -UIConstants.messageCellSpacing),
            chatBubbleView.widthAnchor.constraint(equalToConstant: ViewConstants.chatBubbleSize),
            chatBubbleView.heightAnchor.constraint(equalToConstant: ViewConstants.chatBubbleSize),
            
            contentStackView.topAnchor.constraint(equalTo: chatBubbleView.topAnchor),
            contentStackView.leadingAnchor.constraint(equalTo: chatBubbleView.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: chatBubbleView.trailingAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: chatBubbleView.bottomAnchor),
            
            fullScreenButton.topAnchor.constraint(equalTo: contentImageView.topAnchor),
            fullScreenButton.trailingAnchor.constraint(equalTo: contentImageView.trailingAnchor),
            fullScreenButton.widthAnchor.constraint(equalToConstant: ViewConstants.fullScreenButtonSize),
            fullScreenButton.heightAnchor.constraint(equalTo: fullScreenButton.widthAnchor),
            
            contentImageView.widthAnchor.constraint(equalTo: chatBubbleView.widthAnchor),
            contentImageView.heightAnchor.constraint(equalTo: chatBubbleView.heightAnchor),
            contentImageView.centerXAnchor.constraint(equalTo: chatBubbleView.centerXAnchor),
            contentImageView.centerYAnchor.constraint(equalTo: chatBubbleView.centerYAnchor),
            
            blurView.topAnchor.constraint(equalTo: contentImageView.topAnchor, constant: ViewConstants.blurViewPadding),
            blurView.bottomAnchor.constraint(equalTo: contentImageView.bottomAnchor, constant: -ViewConstants.blurViewPadding),
            blurView.leadingAnchor.constraint(equalTo: contentImageView.leadingAnchor, constant: ViewConstants.blurViewPadding),
            blurView.trailingAnchor.constraint(equalTo: contentImageView.trailingAnchor, constant: -ViewConstants.blurViewPadding),
        ])
    }
    
    private func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressAttachment))
        chatBubbleView.addGestureRecognizer(tapGesture)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: true)
        alpha = highlighted ? UIConstants.highlightAlpha : 1
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentImageView.image = nil
        contentStackView.removeSubviews()
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(bubbleAnimationView)
        chatBubbleView.heightAnchor.constraint(equalToConstant: ViewConstants.chatBubbleSize).isActive = true
        fullScreenButton.removeEventHandlers()
    }
    
    // MARK: Update state

    private func setContentState(attachment: Attachment) {
        chatBubbleView.setMessageAlignment(attachment.sender.messageAlignment, in: contentView)
        chatBubbleView.backgroundColor = ThemeManager.shared.currentTheme.botMessageBackground
        bubbleAnimationView.stopAnimating()
        blurView.isHidden = true
        
        switch attachment.format {
        case .jpeg, .png:
            displayImage(storagePath: attachment.storagePath)
            titleLabel.isHidden = true
            chatBubbleView.layer.borderWidth = 1
            fullScreenButton.isHidden = false
            fullScreenButton.addAction(UIAction(handler: { [weak self] _ in
                self?.delegate?.didPressAttachmentCell(attachment: .content(attachment: attachment))
            }), for: .primaryActionTriggered)
            
        case .pdf, .txt, .unsupported:
            let attachmentButton = createAttachmentButton(title: attachment.name)
            contentStackView.removeSubviews()
            contentStackView.addArrangedSubview(attachmentButton)
            titleLabel.isHidden = true
            chatBubbleView.backgroundColor = .clear
            chatBubbleView.layer.borderWidth = 0
            fullScreenButton.isHidden = true
            chatBubbleView.heightAnchor.constraint(equalToConstant: ViewConstants.documentButtonHeight).isActive = true
            contentStackView.alignment = .fill
        }
    }
    
    private func setUploadingState(previewImage: UIImage?) {
        chatBubbleView.setMessageAlignment(.sent, in: contentView)
        titleLabel.text = ""
        titleLabel.isHidden = true
        chatBubbleView.layer.borderWidth = 0
        fullScreenButton.isHidden = true
        
        if let previewImage = previewImage {
            contentImageView.image = previewImage
            blurView.isHidden = false
            bubbleAnimationView.color = .white
        } else {
            contentImageView.image = nil
            blurView.isHidden = true
            bubbleAnimationView.color = .black
        }
        bubbleAnimationView.startAnimating()
    }
    
    private func setUploadFailedState() {
        chatBubbleView.setMessageAlignment(.sent, in: contentView)
        chatBubbleView.backgroundColor = UIColor(hex: "#232F41")
        titleLabel.text = .localizedText(key: .uploadFileFailed)
        titleLabel.textColor = .white
        titleLabel.isHidden = false
        blurView.isHidden = true
        bubbleAnimationView.stopAnimating()
        chatBubbleView.layer.borderWidth = 0
        fullScreenButton.isHidden = true
    }

    private func displayImage(storagePath: String) {
        Task {
            let service = AttachmentService()
            if let data = try await service.download(storagePath: storagePath, token: kindlyChatClient.token!) {
                contentImageView.image = UIImage(data: data)
            }
        }
    }
    
    private func createAttachmentButton(title: String) -> ActionButton {
        let actionButton = ActionButton()
        actionButton.setTitleLabel(
            text: title,
            icon: UIImage(named: "icon_attachment", in: .module, with: nil)
        )
        actionButton.isUserInteractionEnabled = false
        return actionButton
    }
    
    // MARK: User Actions

    @objc private func didPressAttachment() {
        delegate?.didPressAttachmentCell(attachment: state)
    }
}

private enum ViewConstants {
    static let blurViewPadding = 20.0
    static let chatBubbleSize = 200.0
    static let cornerRadius = 8.0
    static let documentButtonHeight = 52.0
    static let fullScreenButtonSize = 50.0
}
