import UIKit

/// Displays sent messages.
/// Messages are pinned to the trailing side of the UITableView.
final class SentMessageCell: UITableViewCell {
    private lazy var chatBubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = ThemeManager.shared.currentTheme.userMessageBackground
        view.layer.cornerRadius = UIConstants.messageCornerRadius
        return view
    }()

    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: UIConstants.messageFontSize)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.setContentCompressionResistancePriority(.required, for: .vertical)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        selectionStyle = .none
        backgroundColor = .clear
        contentView.add(views: chatBubbleView)
        chatBubbleView.add(views: messageLabel)
        chatBubbleView.setMessageAlignment(.sent, in: contentView)

        NSLayoutConstraint.activate([
            chatBubbleView.topAnchor.constraint(equalTo: contentView.topAnchor),
            chatBubbleView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -UIConstants.messageCellSpacing),
            chatBubbleView.widthAnchor.constraint(lessThanOrEqualTo: contentView.widthAnchor, multiplier: UIConstants.messageMaxWidthMultiplier),

            messageLabel.topAnchor.constraint(equalTo: chatBubbleView.topAnchor, constant: 10),
            messageLabel.bottomAnchor.constraint(equalTo: chatBubbleView.bottomAnchor, constant: -10),
            messageLabel.leadingAnchor.constraint(equalTo: chatBubbleView.leadingAnchor, constant: 14),
            messageLabel.trailingAnchor.constraint(equalTo: chatBubbleView.trailingAnchor, constant: -14),
        ])
    }

    // MARK: - Update content

    func update(message: ChatMessage) {
        guard let formattedHTML = message.text?.formattedHTML else {
            return
        }
        messageLabel.attributedText = formattedHTML
        // textColor needs to be updated when attributedText is set.
        messageLabel.textColor = ThemeManager.shared.currentTheme.userMessageText
    }
}
