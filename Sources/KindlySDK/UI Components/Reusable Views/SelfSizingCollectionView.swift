import UIKit

/// Custom UICollectionView subclass for use inside a UITableViewCell.
///
/// SelfSizingCollectionView will calculate the largest cell and set UICollectionView height as that value.
final class SelfSizingCollectionView: UICollectionView {
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        // Needs to be called on iOS 14 🤔
        if #unavailable(iOS 15.0) {
            layoutIfNeeded()
        }

        return CGSize(
            width: contentSize.width,
            height: largestCellHeight
        )
    }

    /// Returns the tallest cell in UICollectionView.
    ///
    /// Will return the smallest supported image size (`ImageCarouselSize.small`) if it returns 0.
    private var largestCellHeight: Double {
        let largestCell = visibleCells.max {
            $0.contentView.bounds.height < $1.contentView.bounds.height
        }

        let height = max(
            ImageCarouselSize.small.imageSize,
            (largestCell?.contentView.bounds.height ?? 0)
        )
        return height
    }

    override func reloadData() {
        super.reloadData()
        invalidateIntrinsicContentSize()
        layoutIfNeeded()
    }
}
