import UIKit

/// Custom UITableView that starts scrolling in content from the bottom.
///
/// Chat messages will then be presented at the bottom and then expand upwards.
/// This is done by expanding the contentInset for the free space in the UITableView.
final class ChatTableView: UITableView {
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        DispatchQueue.main.async {
            self.updateContentInsetForPresentingCellsFromBottom()
        }
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Subscribe to updates where we need to change the contentInset.
    
    override func insertRows(at indexPaths: [IndexPath], with animation: UITableView.RowAnimation) {
        super.insertRows(at: indexPaths, with: animation)
        updateContentInsetForPresentingCellsFromBottom()
    }
    
    override func insertSections(_ sections: IndexSet, with animation: UITableView.RowAnimation) {
        super.insertSections(sections, with: animation)
        updateContentInsetForPresentingCellsFromBottom()
    }
    
    override func reloadData() {
        super.reloadData()
        updateContentInsetForPresentingCellsFromBottom()
    }
    
    override func performBatchUpdates(_ updates: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
        super.performBatchUpdates(updates, completion: completion)
        updateContentInsetForPresentingCellsFromBottom()
    }
    
    // MARK: Calculate height
    
    private func updateContentInsetForPresentingCellsFromBottom() {
        var calculatedContentInset = bounds.size.height
        
        for section in 0..<numberOfSections {
            for row in 0..<numberOfRows(inSection: section) {
                let rowHeight = rectForRow(at: IndexPath(row: row, section: section)).size.height
                calculatedContentInset -= rowHeight
                if calculatedContentInset <= 0 {
                    break
                }
            }
        }
        
        let contentInsetTop = max(16, calculatedContentInset)
        if contentInset.top == contentInsetTop {
            return
        }
        contentInset.top = contentInsetTop
        contentInset.bottom = 0
    }
}
