import UIKit

/// Input Text View with support for placeholder and dynamic height.
@available(iOS 14.0, *)
final class InputTextView: UITextView {
    // MARK: Placeholder label

    var inputPlaceholderLabel: UILabel = {
        let label = UILabel()
        label.textColor = ThemeManager.shared.currentTheme.inputText?.withAlphaComponent(0.6)
        label.isUserInteractionEnabled = false
        return label
    }()

    var placeholderText = "Ask me a question" {
        didSet {
            inputPlaceholderLabel.text = placeholderText
        }
    }

    // MARK: Dynamic height

    var maxHeight: Double = 160 {
        didSet {
            heightConstraint.constant = maxHeight
        }
    }

    private var heightConstraint: NSLayoutConstraint!

    // MARK: Init and view lifecycle

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        add(views: inputPlaceholderLabel)
        addConstraints()
        placeholderText = .localizedText(key: .placeholder)
        font = UIFont.systemFont(ofSize: 16)
        backgroundColor = ThemeManager.shared.currentTheme.inputBackground
        textColor = ThemeManager.shared.currentTheme.inputText
        tintColor = ThemeManager.shared.currentTheme.inputCursor
        layer.cornerRadius = ViewConstants.cornerRadius
        textContainerInset = UIEdgeInsets(
            top: ViewConstants.verticalInset,
            left: ViewConstants.horizontalInset,
            bottom: ViewConstants.verticalInset,
            right: ViewConstants.horizontalInset
        )
        isScrollEnabled = false

        NotificationCenter.default.addObserver(forName: UITextView.textDidChangeNotification, object: nil, queue: .main) { [weak self] _ in
            guard let self = self else { return }
            self.heightConstraint.isActive = self.contentSize.height > self.maxHeight
            self.isScrollEnabled = self.contentSize.height > self.maxHeight
            self.invalidateIntrinsicContentSize()
        }
    }

    private func addConstraints() {
        heightConstraint = heightAnchor.constraint(equalToConstant: maxHeight)

        NSLayoutConstraint.activate([
            inputPlaceholderLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            inputPlaceholderLabel.leadingAnchor.constraint(
                equalTo: leadingAnchor,
                constant: ViewConstants.horizontalInset + ViewConstants.placeholderExtraInset
            ),
            inputPlaceholderLabel.trailingAnchor.constraint(
                equalTo: trailingAnchor,
                constant: -ViewConstants.horizontalInset + ViewConstants.placeholderExtraInset
            ),
        ])
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

private enum ViewConstants {
    static let cornerRadius = 18.0
    static let horizontalInset = 14.0
    static let placeholderExtraInset = 4.0
    static let verticalInset = 9.0
}
