import UIKit

/// Main action button in the SDK.
@available(iOS 14.0, *)
final class ActionButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(
            roundedRect: bounds,
            cornerRadius: ViewConstants.cornerRadius
        ).cgPath
    }
    
    override var isHighlighted: Bool {
        didSet {
            alpha = isHighlighted ? 0.9 : 1
        }
    }
    
    // MARK: Setup
    
    private func setup() {
        backgroundColor = ThemeManager.shared.currentTheme.buttonBackground
        tintColor = ThemeManager.shared.currentTheme.buttonText
        setTitleColor(ThemeManager.shared.currentTheme.buttonText, for: .normal)
        contentEdgeInsets = UIEdgeInsets(
            top: ViewConstants.verticalInset,
            left: ViewConstants.horizontalInset,
            bottom: ViewConstants.verticalInset,
            right: ViewConstants.horizontalInset
        )
        layer.cornerRadius = ViewConstants.cornerRadius
        layer.borderWidth = ViewConstants.borderWidth
        layer.borderColor = ThemeManager.shared.currentTheme.buttonOutline?.cgColor
        titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        titleLabel?.numberOfLines = 0
        titleLabel?.lineBreakMode = .byTruncatingTail
        titleLabel?.textAlignment = .center
        layer.shadowColor = ThemeManager.shared.currentTheme.defaultShadow?.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.5
    }
    
    // MARK: Actions

    func setSelected() {
        backgroundColor = ThemeManager.shared.currentTheme.buttonSelectedBackground
    }
    
    func setTitleLabel(text: String, icon: UIImage?) {
        guard let icon = icon else {
            return
        }

        let title = NSMutableAttributedString(string: text + " ")
        let logoAttachment = NSTextAttachment(
            image: icon.withTintColor(
                ThemeManager.shared.currentTheme.buttonText ?? .black
            )
        )
        logoAttachment.adjustsImageSizeForAccessibilityContentSizeCategory = true
        title.append(NSAttributedString(attachment: logoAttachment))
        setAttributedTitle(title, for: .normal)
    }
}

private enum ViewConstants {
    static let borderWidth = 1.0
    static let cornerRadius = 7.0
    static let horizontalInset = 16.0
    static let verticalInset = 14.0
}
