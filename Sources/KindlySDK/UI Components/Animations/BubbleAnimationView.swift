import UIKit

/// Custom loading view with 3 animated bubbles.
final class BubbleAnimationView: UIView {
    internal var color = UIColor.black {
        didSet {
            updateColors()
        }
    }

    private var bubbleSize: Double = 8.0
    private var bounceDistance: Double = 6.0
    private let animationDuration = 0.4
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 4
        return stackView
    }()
    
    private let typingBubbles: [CircleView] = [
        CircleView(), CircleView(), CircleView(),
    ]
    
    init(color: UIColor = .black, bubbleSize: Double = 8.0, bounceDistance: Double = 6.0) {
        self.color = color
        self.bubbleSize = bubbleSize
        self.bounceDistance = bounceDistance
        super.init(frame: .zero)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        add(views: stackView)
        addConstraints()
        typingBubbles.forEach {
            $0.backgroundColor = color
            $0.widthAnchor.constraint(equalToConstant: bubbleSize).isActive = true
            $0.heightAnchor.constraint(equalTo: $0.widthAnchor).isActive = true
            stackView.addArrangedSubview($0)
        }
        startAnimating()
    }
    
    private func addConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
    private func updateColors() {
        typingBubbles.forEach {
            $0.backgroundColor = color
        }
    }
    
    // MARK: Animation actions
    
    func startAnimating() {
        isHidden = false
        
        let delayDuration = 0.2
        let totalDuration = (delayDuration + animationDuration * 2)
        
        let bounceRelativeDuration: Double = animationDuration / totalDuration
        let bounceRelativeTime: Double = delayDuration / totalDuration
        let fallRelativeTime: Double = (delayDuration + animationDuration) / totalDuration
        
        for (index, circle) in stackView.arrangedSubviews.enumerated() {
            let delay = animationDuration * 2.2 * TimeInterval(index) / TimeInterval(stackView.arrangedSubviews.count)
            
            // Inspired by: https://stackoverflow.com/a/62210326
            UIView.animateKeyframes(withDuration: totalDuration, delay: delay, options: [.repeat], animations: {
                UIView.addKeyframe(withRelativeStartTime: bounceRelativeTime, relativeDuration: bounceRelativeDuration) {
                    circle.transform = CGAffineTransform(translationX: 0, y: -self.bounceDistance)
                    circle.alpha = 1
                }
                
                UIView.addKeyframe(withRelativeStartTime: fallRelativeTime, relativeDuration: bounceRelativeDuration) {
                    circle.transform = CGAffineTransform(translationX: 0, y: 0)
                    circle.alpha = 0.4
                }
            })
        }
    }
    
    func stopAnimating() {
        for bubbleView in stackView.arrangedSubviews {
            UIView.animate(withDuration: 0.1) {
                bubbleView.alpha = 0
            }
        }
        isHidden = true
    }
}

private final class CircleView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        alpha = 0.3
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.width / 2
    }
}
