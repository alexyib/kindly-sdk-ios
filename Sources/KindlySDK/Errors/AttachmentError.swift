/// Error cases for attachment support.
enum AttachmentError: Error {
    case googleUploadFailed
    case invalidFormat
    case invalidUploadURL
    case networkConnection
    case rejected
}
