/// Error cases when interacting with Kindly Chat API.
public enum KindlySDKError: Error {
    case configNotSet
    case connectionIsInProgress
    case general
    case httpError(statusCode: Int)
    case invalidURL
    case noInternetConnection
    case tokenInvalid
    case tokenMissing
}
