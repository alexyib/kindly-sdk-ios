# Localization

Get started with localization.

![Screenshot of the localization](screenshot-localization.png)

## Overview

- Kindly SDK support localized UI and chat content.
- Available languages are defined in `app.kindly.ai`. 
- Users can change language in "Chat settings".

## Getting supported languages

A list of supported languages can be retrieved after the SDK has been initialized. The list contains ``Language`` objects. You can use ``Language``'s `languageCode` when connecting.

```swift
let languages = KindlyChatClient.shared.languages
```

## Setting a language

You can easily specify a language code to request a specific language.

**When presenting the chat UI:**

```swift
let chatViewController = KindlyChatNavigationController(languageCode: "LANGUAGE_CODE/nil")
present(chatViewController, animated: true)
```

**When manually connecting to the chat API:**
```swift 
KindlyChatClient.shared.connect(languageCode: "fr")
```

- Important: If the requested language is not available, then English will be used as fallback.
