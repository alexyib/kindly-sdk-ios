# ``KindlySDK``

Integrate Kindly Chat in your iOS app.

## Overview

![Screenshot of the Kindly Chat UI](screenshot-main.png)

### Getting started

- A list of supported features can be found here: <doc:Features>.
- Read more about localization here: <doc:Localization>.

### Configure your app

#### 1: Initialize Kindly SDK

- Create a file named `KindlyChat.swift`. 
- Use the template below to create a shared instance of ``KindlyChatClient``. 
- Insert API keys in this file.

```swift
import KindlySDK

extension KindlyChatClient {
    static let shared: KindlyChatClient = {
        let config = KindlySDKConfig(botKey: "BOT_KEY")
        return KindlyChatClient(config: config)
    }()
}
```

- Tip: See ``KindlySDKConfig`` for more info about ways to configure the SDK.

#### 2: Load configuration from Kindly

- Call `loadConfigFromKindly()` early in your app's lifecycle. For example in `AppDelegate -> didFinishLaunchingWithOptions`.
- This will fetch bot settings, colors and localizations configured in `app.kindly.ai`.
- Configuration must be loaded before starting a chat.

```swift
KindlyChatClient.shared.loadConfigFromKindly()
```

#### 3: Present chat view

- The chat view is designed to be presented modally.
- You can specify a language when presenting the chat view. Learn more how to find supported languages in this article: <doc:Localization>
- ``KindlyChatNavigationController`` will connect to the Kindly API and start a chat session. Read more about preloading a conversation in this document.

**UIKit**

```swift
let chatViewController = KindlyChatNavigationController(languageCode: "LANGUAGE_CODE/nil")
present(chatViewController, animated: true)
```

**SwiftUI**

Present ``KindlyChatNavigationController`` in a `UIViewControllerRepresentable`. The demo app provides an example.

### Additional features

#### End the active conversation

There are two ways to end a chat session:

- The user selects "Cancel chat"
- The client app can call the `endActiveChat()` function.

```swift
KindlyChatClient.shared.endActiveChat()
```

#### Handover

If chat handover will be handled outside of Kindly, you can subscribe to the following notification:

```swift
NSNotification.Name("KindlySDK-Handover")
```

This `NSNotification` will be posted immediately when the user requests handover. If handover is handled outside of Kindly, you can then end the active chat. 

#### Subscribe to events

``KindlyChatClient`` publishes updates using `@Published` from Combine. Your app can subscribe to changes for current bot and connection state.

```swift
@Published var connectionState: [ChatSessionState]
@Published var currentBot: Bot?
```

#### Preload conversation

- You can preload the chat for performance reasons. Messages will then be ready when you open the chat. 
- Use the `connect()` function from ``KindlyChatClient``. You can specify a preferred ``LanguageCode``.  

Example 1: Using the Result type in Swift

```swift
KindlyChatClient.shared.connect(languageCode: "LANGUAGE_CODE/nil") { result in
    switch result {
    case .success:
        // Present the chat UI view.
    case .failure(let failure):
        // Handle error.
    }
}
```

Example 2: Using async/await
```swift
do {
    try await KindlyChatClient.shared.connect(languageCode: "LANGUAGE_CODE/nil")
    // Present the chat UI view.
} catch {
    // Handle error.
}
```

## Topics

### Introduction

- <doc:Features>
- <doc:Localization>

### Chat Client

- ``KindlyChatClient``
- ``KindlySDKConfig``
- ``ServerEnvironment``
- ``ChatConnectionState``

### User Interface

- ``KindlyChatNavigationController``

### Models

- ``Bot``
- ``UserMessage``

### Localization

- ``Language``
- ``LanguageCode``

### Errors

- ``KindlySDKError``
