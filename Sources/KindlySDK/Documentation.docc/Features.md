# Features

Currently supported features in the SDK.

## Message types

The following message types are supported in the SDK.

* Message
* Button
* Image
* Video

## Button interaction types

| Button Type | Status |
| ------------ | ------------------------------------- | 
| External link  | Supported |
| Trigger another dialogue | Supported |
| Quick reply | Supported |
| Email | Supported |
| Phone | Supported |
| Handover request | Partially supported. Will notify client app to perform handover. |
| Cancel followup | Supported |
| Upload attachment | Supported |
| Export chat conversation | Not supported. Available in Chat settings. |
| Delete chat conversation | Not supported. Available in Chat settings. |
| Leave contact details | Not supported |
| Checkbox | Not supported |
| Submit | Not supported |
| Slider | Not supported |
