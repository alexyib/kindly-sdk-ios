import Foundation

extension Date {
    /// Returns Date in GMT format. This is the same format we receive from Kindly API, making it easy to compare local and remote dates.
    static var gmt: Date {
        let currentDate = Date()
        let seconds = -TimeInterval(TimeZone.current.secondsFromGMT(for: currentDate))
        return Date(timeInterval: seconds, since: currentDate)
    }
}
