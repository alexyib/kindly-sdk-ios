import Foundation

extension String {
    /// Converts HTML content to NSAttributedString. Supports font styling, lists and more.
    var formattedHTML: NSAttributedString? {
        // Convert newline characters to HTML so we can support multiline text.
        let text = replacingOccurrences(of: "\n", with: "<br>")
        // Inject `font-size` and `font-family` to set the -apple-system font.
        let formattedHTML = String(format: "<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(UIConstants.messageFontSize)\">%@</span>", text)

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]

        guard let html = try? NSAttributedString(
            data: Data(formattedHTML.utf8),
            options: options,
            documentAttributes: nil
        ) else {
            return nil
        }
        return html
    }
    
    /// Convenient extension for getting a localized string.
    @available(iOS 14.0, *)
    static func localizedText(key: LocalizationKey, languageCode: LanguageCode? = nil) -> String {
        return LocalizationUtil().getString(for: key, languageCode: languageCode)
    }
}
