import Foundation

@available(iOS 14.0, *)
extension URLSession {
    /// Extension for supporting async/await requests on older iOS versions.
    @available(iOS, deprecated: 15.0, message: "This extension is no longer necessary. Use API built into SDK")
    func data(from request: URLRequest) async throws -> (Data?, URLResponse) {
        try await withCheckedThrowingContinuation { continuation in
            let task = dataTask(with: request) { data, response, error in
                guard let response = response else {
                    let error = error ?? URLError(.badServerResponse)
                    return continuation.resume(throwing: error)
                }
                return continuation.resume(returning: (data, response))
            }
            task.resume()
        }
    }

    /// Extension for supporting async/await downloads on older iOS versions.
    ///
    /// - Parameter saveToURL: Destination where the downloaded file should be stored.
    func download(request: URLRequest, saveToURL: URL) async throws -> URL {
        try await withCheckedThrowingContinuation { continuation in
            let downloadTask = downloadTask(with: request) { fileURL, _, _ in
                guard let fileURL = fileURL else {
                    continuation.resume(throwing: KindlySDKError.invalidURL)
                    return
                }

                do {
                    if FileManager.default.fileExists(atPath: saveToURL.path) {
                        try FileManager.default.removeItem(at: saveToURL)
                    }
                    try FileManager.default.moveItem(at: fileURL, to: saveToURL)
                } catch {
                    continuation.resume(throwing: error)
                    return
                }
                
                continuation.resume(returning: saveToURL)
            }
            downloadTask.resume()
        }
    }
}
