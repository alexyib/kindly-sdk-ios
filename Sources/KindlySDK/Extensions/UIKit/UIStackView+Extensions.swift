import UIKit

extension UIStackView {
    /// Convenient way to clear all subviews in `UIStackView`.
    func removeSubviews() {
        let subviewsToRemove = arrangedSubviews.reduce([]) { allSubviews, subview -> [UIView] in
            removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        subviewsToRemove.forEach { $0.removeFromSuperview() }
    }
}
