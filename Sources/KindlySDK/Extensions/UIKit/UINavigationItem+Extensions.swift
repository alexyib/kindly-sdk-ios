import UIKit

extension UINavigationItem {
    /// A convenient way for setting both title and image in a UINavigationBar.
    func setImageTitleView(image: UIImage, title: String, size: Double = 28) {
        let titleView = UIStackView()
        titleView.axis = .horizontal
        titleView.spacing = 8
        titleView.alignment = .center

        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = .preferredFont(forTextStyle: .headline)
        titleLabel.textColor = ThemeManager.shared.currentTheme.navBarText

        let imageView = UIImageView(image: image)
        imageView.layer.cornerRadius = 8
        imageView.clipsToBounds = true

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: size),
            imageView.heightAnchor.constraint(equalToConstant: size),
        ])

        titleView.addArrangedSubview(imageView)
        titleView.addArrangedSubview(titleLabel)
        self.backButtonTitle = title
        self.titleView = titleView
    }
}
