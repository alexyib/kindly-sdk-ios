import UIKit

@available(iOS 14.0, *)
extension UIControl {
    // Clears all added actions from UIControl.
    func removeEventHandlers() {
        enumerateEventHandlers { action, _, event , _ in
            if let action = action {
                removeAction(action, for: event)
            }
        }
    }
}
