import UIKit

extension UITableView {
    /// Registers multiple UITableViewCells at once.
    func register(_ cells: UITableViewCell.Type...) {
        cells.forEach { cell in
            register(cell, forCellReuseIdentifier: cell.identifier)
        }
    }
    
    /// Determines if UITableView can scroll to the IndexPath.
    func canScrollTo(indexPath: IndexPath) -> Bool {
        guard numberOfSections > 0 else {
            return false
        }
        
        let lastSection = numberOfSections - 1
        let lastRow = numberOfRows(inSection: indexPath.section) - 1
        
        guard lastSection >= 0, lastRow >= 0 else {
            return false
        }
        
        guard indexPath.section <= lastSection else {
            return false
        }
        
        guard indexPath.row <= lastRow else {
            return false
        }
        
        guard indexPath.row >= 0 else {
            return false
        }
        
        guard indexPath.section >= 0 else {
            return false
        }
        
        return true
    }
    
    /// Returns the last indexPath in the UITableView.
    var lastIndexPath: IndexPath? {
        guard numberOfSections > 0 else {
            return nil
        }
        
        let section = max(numberOfSections - 1, 0)
        let row = max(numberOfRows(inSection: section) - 1, 0)
        return IndexPath(row: row, section: section)
    }
}
