import UIKit

/// A convenient way to get a reusable view's identifier.
///
/// Useful when registering UITableViewCells and UICollectionViewCells.
protocol ReusableView: AnyObject {
    static var identifier: String { get }
}

extension ReusableView where Self: UIView {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UICollectionViewCell: ReusableView {}
extension UITableViewCell: ReusableView {}
