import OSLog
import UIKit.UIImage

@available(iOS 14.0, *)
extension UIImageView {
    /// Load image from remote URL.
    func setAsyncImage(from url: URL) {
        AsyncImageService().fetchImage(for: url) { [weak self] result in
            switch result {
            case .success(let image):
                self?.image = image
            case .failure(let error):
                Logger.shared.log(level: .error, "Failed to load image at \(url): \(error)")
                self?.image = nil
            }
        }
    }
}
