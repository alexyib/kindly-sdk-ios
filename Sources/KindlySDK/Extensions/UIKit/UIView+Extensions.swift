import UIKit

extension UIView {
    /// Add multiple Auto Layout subviews at the same time.
    func add(views: UIView...) {
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(view)
        }
    }

    /// Extension for aligning message bubbles in the chat view.
    func setMessageAlignment(_ alignment: MessageAlignment, in contentView: UIView) {
        switch alignment {
        case .received:
            NSLayoutConstraint.activate([
                leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: UIConstants.horizontalMargin),
                trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -UIConstants.horizontalMargin)
            ])
        case .sent:
            NSLayoutConstraint.activate([
                leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: UIConstants.horizontalMargin),
                trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -UIConstants.horizontalMargin)
            ])
        }
    }
}
