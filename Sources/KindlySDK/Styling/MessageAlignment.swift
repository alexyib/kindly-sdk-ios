/// Alignment for messages presented in the chat view.
enum MessageAlignment {
    case received
    case sent
}
