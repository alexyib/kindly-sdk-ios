import UIKit.UIColor

protocol Theme {
    var background: UIColor? { get }
    var botMessageBackground: UIColor? { get }
    var botMessageText: UIColor? { get }
    var buttonBackground: UIColor? { get }
    var buttonOutline: UIColor? { get }
    var buttonSelectedBackground: UIColor? { get }
    var buttonText: UIColor? { get }
    var defaultShadow: UIColor? { get }
    var inputBackground: UIColor? { get }
    var inputCursor: UIColor? { get }
    var inputText: UIColor? { get }
    var navBarBackground: UIColor? { get }
    var navBarText: UIColor? { get }
    var userMessageBackground: UIColor? { get }
    var userMessageText: UIColor? { get }
}

/// Default theme.
///
/// Contains fallback values.
struct KindlyTheme: Theme {
    var background: UIColor? {
        UIColor(hex: "#FFFFFF")
    }

    var botMessageBackground: UIColor? {
        UIColor(hex: "#F1F4F8")
    }

    var botMessageText: UIColor? {
        UIColor(hex: "#232F41")
    }

    var buttonBackground: UIColor? {
        UIColor(hex: "#FFFFFF")
    }

    var buttonOutline: UIColor? {
        UIColor(hex: "#000000")?.withAlphaComponent(0.08)
    }

    var buttonSelectedBackground: UIColor? {
        UIColor(hex: "#000000")?.withAlphaComponent(0.18)
    }

    var buttonText: UIColor? {
        UIColor(hex: "#000000")
    }

    var defaultShadow: UIColor? {
        UIColor.black.withAlphaComponent(0.08)
    }

    var inputBackground: UIColor? {
        UIColor(hex: "#F4F7FA")
    }

    var inputCursor: UIColor? {
        UIColor(hex: "#1CD300")
    }

    var inputText: UIColor? {
        UIColor(hex: "#0C141F")
    }

    var navBarBackground: UIColor? {
        UIColor(hex: "#445265")
    }

    var navBarText: UIColor? {
        UIColor(hex: "#FFFFFF")
    }

    var userMessageBackground: UIColor? {
        UIColor(hex: "#0069FF")
    }

    var userMessageText: UIColor? {
        UIColor(hex: "#FFFFFF")
    }
}

// MARK: Custom Themes

struct CustomTheme: Theme {
    var background: UIColor?
    var botMessageBackground: UIColor?
    var botMessageText: UIColor?
    var buttonBackground: UIColor?
    var buttonOutline: UIColor?
    var buttonSelectedBackground: UIColor?
    var buttonText: UIColor?
    var defaultShadow: UIColor?
    var inputBackground: UIColor?
    var inputCursor: UIColor?
    var inputText: UIColor?
    var navBarBackground: UIColor?
    var navBarText: UIColor?
    var userMessageBackground: UIColor?
    var userMessageText: UIColor?
}
