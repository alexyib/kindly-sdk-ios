import Foundation

enum UIConstants {
    static let highlightAlpha = 0.6
    static let horizontalMargin = 20.0
    static let messageCellSpacing = 14.0
    static let messageCornerRadius = 18.0
    static let messageFontSize = 16.0
    static let messageMaxWidthMultiplier = 0.66
    static let settingsButtonWidthMultiplier = 0.75
}
