import Foundation

/// A central class to manage Theme support.
final class ThemeManager {
    public static let shared = ThemeManager()

    var currentTheme: Theme

    private init() {
        currentTheme = KindlyTheme()
    }
}
