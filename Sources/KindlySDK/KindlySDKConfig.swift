import Foundation

/// Setup the Kindly SDK with your configuration.
///
/// - Parameter botKey: The key for your application. See [the Kindly API docs](https://docs.kindly.ai/api/application) for more.
/// - Parameter environment: Select the ``ServerEnvironment`` the API should target. Default value is `production`.
public struct KindlySDKConfig {
    /// The Kindly bot key for your application.
    let botKey: String

    /// The key for configuring Pusher. Initialised with a default value.
    let pusherKey: String

    /// The cluster Pusher will connect to. For example "eu". Initialised with a default value.
    let cluster: String

    /// Select which server environment the SDK should target.
    let environment: ServerEnvironment

    public typealias AuthTokenCallback = (_ chatId: String) -> (String)
    
    let getAuthToken: AuthTokenCallback?
    
    public init(
        botKey: String,
        environment: ServerEnvironment = ServerEnvironment.production,
        authTokenCallback: AuthTokenCallback? = nil
    ) {
        self.botKey = botKey
        self.environment = environment
        self.pusherKey = "0d15baa2a7308d71e79d"
        self.cluster = "eu"
        self.getAuthToken = authTokenCallback
    }
}

/// Select the environment for the SDK.
/// - Note: This is only intented for development purposes.
public enum ServerEnvironment {
    /// Points to `bot-dev.kindly.ai`
    case dev
    /// Points to `bot.kindly.ai`
    case production

    var formattedDescription: String {
        switch self {
        case .dev: return "Development"
        case .production: return "Production"
        }
    }
}
