import OSLog

@available(iOS 14.0, *)
extension Logger {
    private static let subsystem = "no.kindly.chat.sdk"
    static let shared = Logger(subsystem: subsystem, category: "KindlySDK")
}
