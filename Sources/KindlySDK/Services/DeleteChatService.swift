import Foundation
import OSLog

/// Request Kindly API to delete a chat conversation.
@available(iOS 14.0, *)
final class DeleteChatService {
    private let httpClient: HTTPClientProtocol

    init(httpClient: HTTPClientProtocol = HTTPClient()) {
        self.httpClient = httpClient
    }

    /// Perform request to delete a chat conversation.
    ///
    /// - Parameter token: A valid token for the conversation to be deleted.
    func deleteChatLog(token: String) {
        let headers: [HTTPHeader] = [
            .kindlyChatBubbleToken(value: token)
        ]

        Task {
            do {
                try await httpClient.request(endpoint: .deleteChatLog, headers: headers)
            } catch {
                Logger.shared.log(level: .error, "Failed to delete chat: \(error)")
            }
        }
    }
}
