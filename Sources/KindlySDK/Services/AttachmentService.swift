import Foundation
import OSLog

@available(iOS 14.0, *)
protocol AttachmentServiceProtocol {
    func upload(request: AttachmentRequest, botID: String, chatID: String, token: String) async throws
    func download(storagePath: String, token: String) async throws -> Data?
}

internal struct AttachmentRequest: Hashable {
    let payload: Data
    let filename: String
    let format: FileFormat
}

/// Upload or download attachments in Kindly API.
@available(iOS 14.0, *)
final class AttachmentService: AttachmentServiceProtocol {
    private let httpClient: HTTPClientProtocol

    init(httpClient: HTTPClientProtocol = HTTPClient()) {
        self.httpClient = httpClient
    }

    // MARK: - Upload

    /// Create request for uploading an attachment to Kindly.
    ///
    /// Uploading is a multi-step process. See each step for description.
    ///
    /// - Parameter request: AttachmentRequest containing payload, filename and file format.
    /// - Throws: Will throw AttachmentError.invalidFormat if FileFormat value is `unsupported`.
    internal func upload(request: AttachmentRequest, botID: String, chatID: String, token: String) async throws {
        guard request.format != .unsupported else {
            throw AttachmentError.invalidFormat
        }

        let sizeKB = Double(request.payload.count / 1024)

        let convertedAttachment = AttachmentUploadDTO(
            name: request.filename,
            type: request.format.mimeType,
            status: .processing,
            sender: .user,
            temporaryID: UUID().uuidString.lowercased(),
            sizeKB: sizeKB
        )

        let attachmentRequestDTO = AttachmentUploadRequest(
            attachments: [convertedAttachment],
            temporaryChatMessageID: UUID().uuidString.lowercased()
        )

        try await uploadToKindlyWebService(
            attachmentRequest: attachmentRequestDTO,
            payload: request.payload,
            botID: botID,
            chatID: chatID,
            token: token
        )
    }

    /// Upload attachment to Kindly Chat API.
    private func uploadToKindlyWebService(attachmentRequest: AttachmentUploadRequest, payload: Data, botID: String, chatID: String, token: String) async throws {
        let headers: [HTTPHeader] = [
            .kindlyChatBubbleToken(value: token),
            .contentType(value: "application/json")
        ]

        do {
            let encodedAttachment = try JSONEncoder().encode(attachmentRequest)
            let responseData = try await httpClient.request(endpoint: .attachmentCreate, headers: headers, body: encodedAttachment)
            let responseDTO = try JSONDecoder().decode(AttachmentUploadResponse.self, from: responseData!)

            for attachment in responseDTO.attachments {
                guard let uploadURL = URL(string: attachment.uploadURL ?? "") else {
                    throw AttachmentError.invalidUploadURL
                }

                // MARK: Upload data to GCS

                try await uploadToGCS(uploadURL: uploadURL, payload: payload, filetype: attachment.type, botID: botID, chatID: chatID)

                // MARK: Notify Kindly the upload finished successfully

                try await notifyUploadSuccess(
                    temporaryChatMessageID: attachmentRequest.temporaryChatMessageID,
                    attachmentID: attachment.id,
                    token: token
                )
            }
        } catch {
            Logger.shared.log(level: .error, "Failed to upload file. Error message: \(error)")
            throw error
        }
    }

    /// Upload file to Google Cloud Services.
    ///
    /// - Parameter uploadURL: Received URL from attachment/create in Kindly API.
    /// - Parameter payload: Data to upload. Same payload data as in previous step.
    /// - Parameter filetype: MIME file format.
    private func uploadToGCS(uploadURL: URL, payload: Data, filetype: String, botID: String, chatID: String) async throws {
        let headers: [HTTPHeader] = [
            HTTPHeader(field: "x-goog-upload-bot-id", value: botID),
            HTTPHeader(field: "x-goog-upload-chat-id", value: chatID),
            HTTPHeader.contentType(value: filetype)
        ]

        do {
            try await httpClient.request(from: uploadURL, method: .PUT, headers: headers, body: payload)
        } catch {
            throw AttachmentError.googleUploadFailed
        }
    }

    /// Notify Kindly the upload succeeded.
    ///
    /// - Parameter temporaryChatMessageID: Identifier set in first step (attachment/create).
    /// - Parameter attachmentID: Handle attachment with the attachment set in first step.
    private func notifyUploadSuccess(temporaryChatMessageID: String, attachmentID: String, token: String) async throws {
        let uploadCompletedHeaders = [
            HTTPHeader.kindlyChatBubbleToken(value: token),
            HTTPHeader.contentType(value: "application/json")
        ]

        let uploadSuccessDTO = UploadSucceededDTO(
            attachmentIds: [attachmentID],
            temporaryChatMessageId: temporaryChatMessageID,
            exchangeId: UUID().uuidString.lowercased()
        )

        let uploadSuccessData = try JSONEncoder().encode(uploadSuccessDTO)

        try await httpClient.request(
            endpoint: .uploadsComplete,
            headers: uploadCompletedHeaders,
            body: uploadSuccessData
        )
    }

    // MARK: - Download

    /// Download an attachment.
    ///
    /// Attachments are cached with URLCache in HTTPClient.
    /// - Parameter storagePath: Received when requesting an attachment response from the backend.
    func download(storagePath: String, token: String) async throws -> Data? {
        let headers: [HTTPHeader] = [
            .kindlyChatBubbleToken(value: token)
        ]
        let data = try await httpClient.request(
            endpoint: .attachment(storagePath: storagePath),
            headers: headers,
            shouldCacheResponse: true
        )
        return data
    }
}

// MARK: - DTOs

// MARK: Upload request

private struct AttachmentUploadRequest: Encodable {
    let attachments: [AttachmentUploadDTO]
    let temporaryChatMessageID: String

    enum CodingKeys: String, CodingKey {
        case attachments
        case temporaryChatMessageID = "temporaryChatMessageId"
    }
}

private struct AttachmentUploadDTO: Encodable {
    let name: String
    let type: String
    let status: AttachmentStatus
    let sender: ChatMessageSender
    let temporaryID: String
    let sizeKB: Double

    enum CodingKeys: String, CodingKey {
        case name, type, status, sender
        case temporaryID = "temporaryId"
        case sizeKB = "sizeKb"
    }
}

// MARK: Received response for requested attachment

private struct AttachmentUploadResponse: Decodable {
    /// A list of attachments we have declared and are allowed to upload.
    let attachments: [AttachmentResponseDTO]
}

internal struct AttachmentResponseDTO: Decodable {
    let name: String
    let type: String
    let status: String
    let sender: String
    let temporaryID: String
    let sizeKB: Int
    let storagePath: String
    let id: String
    let uploadURL: String?

    enum CodingKeys: String, CodingKey {
        case name, type, status, sender
        case temporaryID = "temporary_id"
        case sizeKB = "size_kb"
        case storagePath = "storage_path"
        case id
        case uploadURL = "upload_url"
    }

    func toAttachment() -> Attachment {
        return Attachment(
            id: id,
            name: name,
            format: FileFormat(mimeType: type),
            status: AttachmentStatus(rawValue: status) ?? .unknown,
            sender: ChatMessageSender(rawValue: sender) ?? .unknown,
            storagePath: storagePath
        )
    }
}

// MARK: Received attachment in the chat

internal struct ReceivedAttachmentDTO: Decodable {
    let attachments: [AttachmentResponseDTO]
    let chatMessageId: String
}

// MARK: Upload succeeded

/// This will be posted to Kindly API when the payload has completed uploading to GCS.
private struct UploadSucceededDTO: Encodable {
    let attachmentIds: [String]
    let temporaryChatMessageId: String
    let exchangeId: String
}
