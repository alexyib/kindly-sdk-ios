import Foundation
import OSLog
import UIKit.UIColor

@available(iOS 14.0, *)
protocol ConfigServiceProtocol {
    func fetchKindlyConfiguration(for botKey: String) async throws -> (languages: [Language], botID: String)
}

/// Fetch configuration for the chat client.
///
/// Configuration includes colors, localization strings and supported languages.
/// This service will fetch the values defined in Kindly (app.kindly.ai).
@available(iOS 14.0, *)
final class ConfigService: ConfigServiceProtocol {
    private let httpClient: HTTPClientProtocol
    private let themeManager = ThemeManager.shared

    init(httpClient: HTTPClientProtocol) {
        self.httpClient = httpClient
    }

    func fetchKindlyConfiguration(for botKey: String) async throws -> (languages: [Language], botID: String) {
        let data = try await httpClient.request(
            from: Endpoint.settingsURL(for: botKey),
            method: .GET
        )
        let configDTO = try JSONDecoder().decode(ConfigDTO.self, from: data!)
        setAppTheme(configDTO.style)
        saveLocalizationFileToDisk(configDTO.text)
        var languages = [Language]()
        configDTO.settings.languages.filter(\.active).forEach { language in
            languages.append(language.toLanguage())
        }
        return (
            languages: languages,
            botID: String(configDTO.settings.id)
        )
    }

    private func setAppTheme(_ style: StyleDTO) {
        let defaultTheme = KindlyTheme()
        themeManager.currentTheme = CustomTheme(
            background: UIColor(hex: style.background),
            botMessageBackground: UIColor(hex: style.botMessageBackground),
            botMessageText: UIColor(hex: style.botMessageTextColor),
            buttonBackground: UIColor(hex: style.buttonBackground),
            buttonOutline: UIColor.black.withAlphaComponent(0.1),
            buttonSelectedBackground: defaultTheme.buttonSelectedBackground,
            buttonText: UIColor(hex: style.buttonText),
            defaultShadow: defaultTheme.defaultShadow,
            inputBackground: UIColor(hex: style.inputBackground),
            inputCursor: defaultTheme.inputCursor,
            inputText: UIColor(hex: style.inputText),
            navBarBackground: UIColor(hex: style.headerBackground),
            navBarText: UIColor(hex: style.headerText),
            userMessageBackground: UIColor(hex: style.userMessageBackground),
            userMessageText: UIColor(hex: style.userMessageTextColor)
        )
    }

    private func saveLocalizationFileToDisk(_ textContent: TextDTO) {
        let localization = LocalizationFile(values: [
            .init(key: .cancelButton, localizations: textContent.cancelButton ?? [:]),
            .init(key: .cancelChatText, localizations: textContent.cancelChatText ?? [:]),
            .init(key: .cancelChatButton, localizations: textContent.cancelChatButton ?? [:]),
            .init(key: .changeLanguageButton, localizations: textContent.changeLanguageButton ?? [:]),
            .init(key: .changeLanguageText, localizations: textContent.changeLanguageText ?? [:]),
            .init(key: .chatDeletedHeader, localizations: textContent.chatDeletedHeader ?? [:]),
            .init(key: .chatDeletedText, localizations: textContent.chatDeletedText ?? [:]),
            .init(key: .confirmActionButton, localizations: textContent.confirmActionButton ?? [:]),
            .init(key: .deleteButton, localizations: textContent.deleteButton ?? [:]),
            .init(key: .deleteText, localizations: textContent.deleteText ?? [:]),
            .init(key: .downloadButton, localizations: textContent.downloadButton ?? [:]),
            .init(key: .downloadFormatText, localizations: textContent.downloadFormatText ?? [:]),
            .init(key: .fileSelectorButton, localizations: textContent.fileSelectorButton ?? [:]),
            .init(key: .photoSelectorButton, localizations: textContent.photoSelectorButton ?? [:]),
            .init(key: .placeholder, localizations: textContent.placeholder ?? [:]),
            .init(key: .settingsButton, localizations: textContent.settingsButton ?? [:]),
            .init(key: .settingsHeader, localizations: textContent.settingsHeader ?? [:]),
            .init(key: .startOverButton, localizations: textContent.startOverButton ?? [:]),
            .init(key: .uploadFileFailed, localizations: textContent.uploadFileFailed ?? [:]),
            .init(key: .uploadFileSubtext, localizations: textContent.uploadFileSubtext ?? [:]),
            .init(key: .uploadFileText, localizations: textContent.uploadFileText ?? [:]),
        ])
        do {
            let configData = try JSONEncoder().encode(localization)
            FileManager.default.createFile(
                atPath: ConfigService.configURL.path,
                contents: configData,
                attributes: nil
            )
        } catch {
            Logger.shared.log(level: .error, "Error saving translations to disk: \(error)")
        }
    }

    /// File location where Kindly config is saved to disk.
    static var configURL: URL {
        let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documents.appendingPathComponent("kindly_config")
    }
}

// MARK: - DTOs

private struct ConfigDTO: Decodable {
    let settings: SettingsDTO
    let style: StyleDTO
    let text: TextDTO
}

private struct SettingsDTO: Decodable {
    let id: Int
    let languages: [LanguageDTO]
}

private struct LanguageDTO: Decodable {
    let code: String
    let name: String
    let active: Bool

    func toLanguage() -> Language {
        Language(code: code, name: name)
    }
}

// MARK: Style

/// StyleDTO contains colors in HEX format.
private struct StyleDTO: Decodable {
    let chatLogElements: String
    let buttonBackground: String
    let bubbleButtonBackground: String
    let buttonText: String
    let background: String
    let headerBackground: String
    let headerText: String
    let inputBackground: String
    let inputText: String
    let userMessageBackground: String
    let userMessageTextColor: String
    let botMessageBackground: String
    let botMessageTextColor: String

    private enum CodingKeys: String, CodingKey {
        case chatLogElements = "color_chat_log_elements"
        case buttonBackground = "color_button_background"
        case bubbleButtonBackground = "color_bubble_button_background"
        case buttonText = "color_button_text"
        case background = "color_background"
        case headerBackground = "color_header_background"
        case headerText = "color_header_text"
        case inputBackground = "color_input_background"
        case inputText = "color_input_text"
        case userMessageBackground = "color_user_message_background"
        case userMessageTextColor = "color_user_message_text_color"
        case botMessageBackground = "color_bot_message_background"
        case botMessageTextColor = "color_bot_message_text_color"
    }
}

// MARK: Text

struct TextDTO: Decodable {
    let cancelButton: [String: String]?
    let cancelChatText: [String: String]?
    let cancelChatButton: [String: String]?
    let changeLanguageButton: [String: String]?
    let changeLanguageText: [String: String]?
    let chatDeletedHeader: [String: String]?
    let chatDeletedText: [String: String]?
    let confirmActionButton: [String: String]?
    let deleteButton: [String: String]?
    let deleteText: [String: String]?
    let downloadButton: [String: String]?
    let downloadFormatText: [String: String]?
    let fileSelectorButton: [String: String]?
    let photoSelectorButton: [String: String]?
    let placeholder: [String: String]?
    let settingsButton: [String: String]?
    let settingsHeader: [String: String]?
    let startOverButton: [String: String]?
    let uploadFileFailed: [String: String]?
    let uploadFileSubtext: [String: String]?
    let uploadFileText: [String: String]?
    
    private enum CodingKeys: String, CodingKey {
        case cancelButton = "cancel_button"
        case cancelChatText = "cancel_chat_text"
        case cancelChatButton = "cancel_chat_button"
        case changeLanguageButton = "change_language_button"
        case changeLanguageText = "change_language_text"
        case chatDeletedHeader = "chat_deleted_header"
        case chatDeletedText = "chat_deleted_text"
        case confirmActionButton = "confirm_action_button"
        case deleteButton = "delete_button"
        case deleteText = "delete_text"
        case downloadButton = "download_button"
        case downloadFormatText = "download_format_text"
        case fileSelectorButton = "ios_file_selector_button"
        case photoSelectorButton = "ios_photo_selector_button"
        case placeholder
        case settingsButton = "settings_button"
        case settingsHeader = "settings_header"
        case startOverButton = "start_over_button"
        case uploadFileFailed = "upload_file_failed"
        case uploadFileSubtext = "upload_file_subtext"
        case uploadFileText = "upload_file_text"
    }
}
