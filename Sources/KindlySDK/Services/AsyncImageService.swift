import UIKit.UIImage

/// Downloads images asynchronously from the requested URL.
@available(iOS 14.0, *)
struct AsyncImageService {
    private let httpClient: HTTPClientProtocol

    init(httpClient: HTTPClientProtocol = HTTPClient()) {
        self.httpClient = httpClient
    }

    func fetchImage(for url: URL, completion: @escaping (Result<UIImage?, Error>) -> Void) {
        Task {
            if let imageData = try? await httpClient.request(from: url, method: .GET, shouldCacheResponse: true) {
                let image = UIImage(data: imageData)
                DispatchQueue.main.async {
                    completion(.success(image))
                }
            } else {
                completion(.failure(KindlySDKError.invalidURL))
            }
        }
    }
}
