import Foundation

/// Property wrapper for dependency injection.
@propertyWrapper
struct Injected<T> {
    var wrappedValue: T

    init() {
        self.wrappedValue = Resolver.resolve()
    }
}
