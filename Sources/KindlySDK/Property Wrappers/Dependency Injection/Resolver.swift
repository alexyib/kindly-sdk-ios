import Foundation

/// Register and load dependencies.
///
/// Inspired by https://holyswift.app/dependency-injection-using-property-wrappers-in-swift/
final class Resolver {
    private var dependencies = [String: AnyObject]()
    private static var shared = Resolver()
    
    static func register<T>(_ dependency: T) {
        shared.register(dependency)
    }
    
    static func resolve<T>() -> T {
        shared.resolve()
    }
    
    private func register<T>(_ dependency: T) {
        let key = String(describing: T.self)
        dependencies[key] = dependency as AnyObject
    }
    
    private func resolve<T>() -> T {
        let key = String(describing: T.self)
        let dependency = dependencies[key] as? T
        
        assert(dependency != nil, "No dependency found")
        
        return dependency!
    }
}
