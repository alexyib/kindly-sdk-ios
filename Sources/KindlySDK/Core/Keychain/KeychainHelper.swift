import Foundation
import Security
import LocalAuthentication
import JWTDecode

public protocol KeychainServiceProtocol {
    static var id: String { get }
    static var keychainService: KeychainService { get }
    static func save(value: String) -> Bool
    static func read() -> String?
    static func delete()
}

public extension KeychainServiceProtocol {
    static var keychainService: KeychainService {
        KeychainService(key: self.id)
    }
    
    static func save(value: String) -> Bool {
        return keychainService.save(text: value)
    }
    
    static func read() -> String? {
        if let value: String = keychainService.read() {
            return value
        }
        
        return nil
    }
    
    static func delete() {
        keychainService.delete()
    }
    
    static func clear() {
        delete()
    }
}

public class KeychainService {
    private var key: String
    
    init(key: String) {
        self.key = key
    }
    
    /// Save a value to the Keychain.
    /// - Parameter text: The value to be saved.
    /// - Returns: Boolean indicating if the save was successful.
    public func save(text: String) -> Bool {
        delete()
        let textData = text.data(using: .utf8)!
        
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: key,
                                    kSecValueData as String: textData]
        let status = SecItemAdd(query as CFDictionary, nil)
        print("Value for key \(key) Added to Keychain: \(status == errSecSuccess)")
        return status == errSecSuccess
    }
    
    /// Read a value from the Keychain.
    /// - Returns: The value stored in the Keychain, or nil if it doesn't exist.
    public func read() -> String? {
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: key,
                                    kSecReturnData as String: true,
                                    kSecMatchLimit as String: kSecMatchLimitOne,
                                    kSecReturnAttributes as String: true, //  If you set the value of kSecReturnAttributes to true, the search results will include the attributes of the Keychain item, such as the creation and modification dates, access control information, and metadata
                                    
                                    // user prompt
                                    kSecUseAuthenticationContext as String: LAContext()]
        
        var item: CFTypeRef?
        let status = SecItemCopyMatching(query as CFDictionary, &item)
        if status == errSecSuccess {
            if let existingItem = item as? [String: Any],
               let textData = existingItem[kSecValueData as String] as? Data,
               let text = String(data: textData, encoding: .utf8) {
                return text
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    /// Delete the value from the Keychain.
    /// - Returns: Boolean indicating if the delete was successful.
    @discardableResult public func delete() -> Bool {
        let query: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: key]
        let status = SecItemDelete(query as CFDictionary)
        return status == errSecSuccess
    }
}

// Keys
extension KeychainService {
    
    /// Auth Token keychain service.
    struct clientJWTToken: KeychainServiceProtocol {
        static var id: String { "KEYCHAIN_CLIENT_AUTH_TOKEN" }
        
        static func read() -> JWT? {
            if let token = keychainService.read() {
                return try? decode(jwt: token)
            }
            
            return nil
        }
        
        static var isExpired: Bool? {
            let jwt: JWT? = self.read()
            guard let jwt = jwt else {
                return nil
            }
            guard let expiredAt = jwt.expiresAt else {
                return true
            }
            
            let expirationThreshold = expiredAt.addingTimeInterval(-30)
            let currentDate = Date()
            return currentDate >= expirationThreshold
        }
    }
}
