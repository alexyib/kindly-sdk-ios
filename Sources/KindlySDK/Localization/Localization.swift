import Foundation

/// LocalizationFile contains all localized strings for the SDK.
///
/// This file should be saved to disk.
struct LocalizationFile: Codable {
    let values: [LocalizationText]
}

/// LocalizationText represents all translations for one ``LocalizationKey`` .
///
/// Values are stored with ``LanguageCode`` as the key.
struct LocalizationText: Codable {
    let key: LocalizationKey
    let localizations: [LanguageCode: String]
}

/// Identifier for a localized string. This enum should contain all user-readable text in the app.
///
/// All strings contains a fallback value in English.
enum LocalizationKey: Codable {
    case cancelButton
    case cancelChatText
    case cancelChatButton
    case changeLanguageButton
    case changeLanguageText
    case chatDeletedHeader
    case chatDeletedText
    case confirmActionButton
    case deleteButton
    case deleteText
    case downloadButton
    case downloadFormatText
    case fileSelectorButton
    case photoSelectorButton
    case placeholder
    case settingsButton
    case settingsHeader
    case startOverButton
    case uploadFileFailed
    case uploadFileSubtext
    case uploadFileText

    /// Fallback value in English.
    var fallback: String {
        switch self {
        case .cancelButton: return "Cancel"
        case .cancelChatText: return "Would you like to cancel the chat?"
        case .cancelChatButton: return "Cancel chat"
        case .changeLanguageButton: return "Change language"
        case .changeLanguageText: return "The chat will reset when you change the language"
        case .chatDeletedHeader: return "Your chat has been deleted."
        case .chatDeletedText: return "The chat closes automatically. Click on the chat bubble to start a new conversation."
        case .confirmActionButton: return "Confirm"
        case .deleteButton: return "Delete chat log"
        case .deleteText: return "Are you sure you want to delete the chat log?"
        case .downloadButton: return "Download chat"
        case .downloadFormatText: return "Select the format best suited for you. Not sure? Choose HTML."
        case .fileSelectorButton: return "Files"
        case .photoSelectorButton: return "Photo Library"
        case .placeholder: return "Ask me a question…"
        case .settingsButton: return "Chat settings"
        case .settingsHeader: return "Chat settings"
        case .startOverButton: return "Start over"
        case .uploadFileFailed: return "Failed to upload\nTry again"
        case .uploadFileSubtext: return "Select source"
        case .uploadFileText: return "Upload file"
        }
    }
}
