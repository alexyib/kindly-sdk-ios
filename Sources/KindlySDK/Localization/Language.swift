import Foundation

/// Represents a language code. For example "en", "fr" or "pl".
public typealias LanguageCode = String

/// Represents a language with local name and code.
public struct Language {
    /// The language code. For example "en", "sv" or "nb".
    public let code: LanguageCode
    /// Formatted language name. For use in "Chat settings" and other places where language is presented to the user.
    public let name: String
}
