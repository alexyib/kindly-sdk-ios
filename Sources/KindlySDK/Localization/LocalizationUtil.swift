import Foundation

@available(iOS 14.0, *)
struct LocalizationUtil {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient
    
    /// Returns a localized string for ``LocalizationKey``.
    func getString(for key: LocalizationKey, languageCode requestLanguageCode: LanguageCode? = nil) -> String {
        do {
            let languageCode: LanguageCode

            if let requestLanguageCode = requestLanguageCode {
                // Set the requested language code if specified.
                languageCode = requestLanguageCode
            } else {
                // Or try to use the currently selected language in `KindlyChatClient`.
                // If no language is selected, then we use the `fallbackLanguageCode`.
                languageCode = currentLanguageCode ?? fallbackLanguageCode
            }
            
            let localizationFile = try LocalizationUtil.loadLocalizationFile()
            let value = localizationFile.values.first(where: { $0.key == key })
            return value?.localizations[languageCode] ?? key.fallback
        } catch {
            return key.fallback
        }
    }
    
    private var currentLanguageCode: LanguageCode? {
        return kindlyChatClient.languageCode
    }
    
    private var fallbackLanguageCode: LanguageCode {
        return "en"
    }
    
    // MARK: Load file from disk

    private static func loadLocalizationFile() throws -> LocalizationFile {
        let data = try Data(contentsOf: ConfigService.configURL)
        let localizationFile = try JSONDecoder().decode(LocalizationFile.self, from: data)
        return localizationFile
    }
}
