import Foundation
import OSLog
import PusherSwift

/// Events that can be subscribed to.
private enum ChatEvent: String {
    /// New or updated attachments. Applies to both sent and received attachments.
    case attachmentsUpdated = "message-attachments-updated"
    /// New messages.
    case message
    /// Successfully subscribed to content.
    case subscriptionSucceeded = "pusher:subscription_succeeded"
    /// Handover requests.
    case takeover
}

/// Connection state for the Kindly Chat API.
public enum ChatConnectionState {
    /// You can now send messages and interact with the API.
    case connected
    /// You need to connect and create a valid session before proceeding.
    case disconnected
    /// Waiting for the connection to be established.
    case isConnecting
    /// The user or client app has ended the conversation.
    case conversationEnded
    /// Connection failed
    case failed
}

internal enum AuthenticationState {
    case none
    case authenticated
    case authenticationError
}

/// iOS SDK for connecting and interacting with the Kindly Chat API.
@available(iOS 14.0, *)
public final class KindlyChatClient: ObservableObject {
    private let httpClient: HTTPClientProtocol
    private let configService: ConfigServiceProtocol
    private var pusher: Pusher?

    /// Client defined settings that includes API keys and more.
    private(set) var config: KindlySDKConfig
    
    /// Token for the active chat conversation.
    private(set) var token: String?
    
    /// Identifier for the active bot.
    private(set) var botID: String?
    
    /// Identifier for the active conversation.
    private(set) var chatID: String?
    
    /// Code for the currently selected language.
    private(set) var languageCode: LanguageCode?
    
    /// Returns a list of supported languages. Populated after the `loadConfigFromKindly()` function has finished.
    public private(set) var languages: [Language] = []
    
    /// Currently set context
    private var localContext: [String: String]? = nil
    
    /// Subscribe to chat messages.
    @Published private(set) var messages: [ChatMessage] = []
    
    /// Subscribe to the currently connected ``Bot``.
    @Published public private(set) var currentBot: Bot?
    
    /// Subscribe to the connection state for ``ChatConnectionState``.
    @Published public internal(set) var connectionState: ChatConnectionState
    
    internal var authenticationState: AuthenticationState

    // MARK: Initialization

    /// Initialize Kindly Chat SDK.
    ///
    /// In most cases, you will only want to have one instance of ``KindlyChatClient``.
    /// Our "Getting started" guide covers this topic and gives you instructions on how to configure the SDK.
    /// - Parameter config: API keys and other configuration of type ``KindlySDKConfig``.
    public init(config: KindlySDKConfig) {
        self.config = config
        connectionState = .disconnected
        authenticationState = .none
        httpClient = HTTPClient()
        configService = ConfigService(httpClient: httpClient)
        // Register values for dependency injections
        Resolver.register(self as KindlyChatClient)
    }
    
    /// Load required config settings from Kindly.
    ///
    /// The function will fetch settings for the conversation as defined in `app.kindly.ai`.
    /// This includes color scheme, localization and supported languages.
    public func loadConfigFromKindly() {
        Logger.shared.log(level: .info, "💬 Initializing Kindly SDK (\(self.config.environment.formattedDescription))")
        Task {
            do {
                let fetchedConfig = try await configService.fetchKindlyConfiguration(for: config.botKey)
                self.botID = fetchedConfig.botID
                self.languages = fetchedConfig.languages
            } catch {
                Logger.shared.log(level: .error, "Failed to load config")
            }
        }
    }
    
    /// Set the current context that will be set in the next message or click request
    /// - Parameter context: context to be set
    public func setNewContext(_ context: [String: String]) {
        self.localContext = context
    }
    
    
    /// Clear the currently set context
    public func clearNewContext() {
        self.localContext = nil
    }
    
    /// Code that will be executed after earch request
    private func performAfterCallDuties() {
        self.clearNewContext()
    }
    
    /// Saves the authentication token in the Keychain.
    ///
    /// - Parameter token: The authentication token to be saved.
    public func saveAuthToken(_ token: String) -> Bool {
        return KeychainService.clientJWTToken.save(value: token)
    }
    
    /// Retrieves the authentication token.
    ///
    /// - Parameter forceNew: A boolean value indicating whether to force the retrieval of a new token.
    /// - Returns: The authentication token if available, otherwise nil.
    internal func getAuthToken(forceNew: Bool = true) -> String? {
        if config.getAuthToken != nil {
            if forceNew {
                return refreshAuthToken()
            } else {
                if let jwtToken = KeychainService.clientJWTToken.read() {
                    if KeychainService.clientJWTToken.isExpired == true {
                        return refreshAuthToken()
                    } else {
                        return jwtToken.string
                    }
                }
            }
        }
        
        return nil
    }
    
    /// Refreshes the authentication token by calling the `getAuthToken` closure provided in the `config` object.
    /// If the callback returns a non-nil token, it is saved in the Keychain and returned.
    /// - Returns: The refreshed authentication token, or nil if the callback returns nil or if saving the token fails.
    internal func refreshAuthToken() -> String? {
        if let authTokenCallback = config.getAuthToken {
            if let chatId = chatID {
                let tempToken: String? = authTokenCallback(chatId)
                if let tempToken = tempToken {
                    if saveAuthToken(tempToken) {
                        return tempToken
                    }
                }
            }
        }
        
        return nil
    }
    
    /// Authenticates the client with the provided auth token.
    /// - Throws: An error if authentication fails.
    internal func authenticateWithAuthToken(forceNew: Bool? = nil) async throws {
        if let authToken = getAuthToken(forceNew: forceNew ?? false), !authToken.isEmpty {
            let headers: [HTTPHeader] = [
                .kindlyBotKey(value: config.botKey),
                .contentType(value: "application/json"),
                .kindlyUserAuthorization(value: authToken)
            ]
            
            do {
                let data = try await httpClient.request(endpoint: .chatAuth, headers: headers)
                authenticationState = .authenticated
            } catch {
                Logger.shared.log(level: .debug, "Failed to authenticate: \(error.localizedDescription)")
                KeychainService.clientJWTToken.delete()
                authenticationState = .authenticationError
                throw error
            }
        }
    }
    
    // MARK: Chat connection
    
    /// Connects to Kindly Chat API and starts a new chat session.
    ///
    /// Behind the scenes, this will return a token for making additional calls to the API.
    /// Requires that config has been set.
    /// - Parameter languageCode: Specify a ``LanguageCode`` to request a specific language. For example "en", "sv" or "nb".
    public func connect(languageCode: LanguageCode? = nil) async throws {
        guard connectionState != .isConnecting else {
            Logger.shared.log(level: .debug, "⏳ Connection is already in progress.")
            throw KindlySDKError.connectionIsInProgress
        }
        
        Logger.shared.log(level: .debug, "⏳ Connecting to Kindly Chat API...")
        DispatchQueue.main.async {
            self.connectionState = .isConnecting
            self.languageCode = languageCode
        }

        let headers: [HTTPHeader] = [
            .kindlyBotKey(value: config.botKey),
            .contentType(value: "application/json")
        ]
        
        var queryItems: [URLQueryItem] = []
        if let languageCode = languageCode {
            queryItems.append(URLQueryItem(name: "language_code", value: languageCode))
        }
        
        var data: Data? = nil
        if let value = self.localContext {
            var connectRequest = ConnectRequestModel()
            connectRequest.context = value
            data = try JSONEncoder().encode(connectRequest)
        }
        do {
            let data = try await httpClient.request(endpoint: .connect, headers: headers, queryItems: queryItems, body: data)
            let dto = try JSONDecoder().decode(ConnectionDTO.self, from: data!)
            token = dto.token
            chatID = dto.chatID
            self.performAfterCallDuties()
            DispatchQueue.main.async {
                self.currentBot = dto.toConnection().bot
                self.connectionState = .connected
            }
            Logger.shared.log(level: .debug, "🟢 Connected to Kindly Chat API")
            await configurePusher(token: dto.token, config: config)
        } catch {
            Logger.shared.log(level: .debug, "Failed to connect: \(error.localizedDescription)")
            DispatchQueue.main.async {
                self.connectionState = .failed
            }
            throw error
        }
    }

    /// Ends the active chat session.
    ///
    /// Messages, token and current connection will be cleared.
    /// This will call the "user left" API in Kindly Chat.
    /// - Note: Please note that the Chat UI will be dismissed after using this function.
    public func endActiveChat() {
        disconnect()

        guard let token = token else {
            return
        }
        let headers: [HTTPHeader] = [
            .kindlyChatBubbleToken(value: token),
            .kindlyBotKey(value: config.botKey)
        ]

        DispatchQueue.main.async {
            self.token = nil
            self.chatID = nil
            self.currentBot = nil
        }

        Task { [headers] in
            do {
                try await httpClient.request(endpoint: .userLeft, headers: headers)
            } catch {
                Logger.shared.log(level: .error, "Failed to end chat \(error)")
            }
        }
    }
    
    /// Restart the conversation.
    ///
    /// The current chat will be ended, and a new will be started automatically right after.
    /// Client apps can request a new language here. If not specified, then the language from the previous conversation will be used.
    /// - Parameter languageCode: Specify the ``LanguageCode`` to request a specific language. For example "en", "sv" or "nb".
    internal func restartConversation(languageCode: LanguageCode? = nil) {
        endActiveChat()
        Task {
            do {
                try await connect(languageCode: languageCode ?? self.languageCode)
            } catch {
                Logger.shared.log(level: .error, "Failed restart conversation: \(error)")
            }
        }
    }

    /// Disconnects from the current session.
    private func disconnect() {
        pusher?.unsubscribeAll()
        pusher?.unbindAll()
        pusher?.disconnect()
        pusher = nil
        DispatchQueue.main.async {
            self.connectionState = .conversationEnded
            self.messages.removeAll()
        }
    }
    
    // MARK: Pusher Config
    
    private func configurePusher(token: String, config: KindlySDKConfig) async {
        pusher = Pusher(key: config.pusherKey, options: .init(
            authMethod: .authRequestBuilder(authRequestBuilder: AuthRequestBuilder()),
            host: .cluster(config.cluster)
        ))

        pusher?.connect()
        let channel = "private-chatbubble-\(token)"
        subscribeToChatEvents(in: channel)
    }
    
    // MARK: Handle chat events
    
    /// Subscribe to chat events.
    ///
    /// We use Pusher for subscribing to events in the chat.
    /// Supported event types are defined in ``ChatEvent``.
    private func subscribeToChatEvents(in channelName: String) {
        _ = pusher?.subscribe(channelName: channelName)
        pusher?.bind { [weak self] event in
            Logger.shared.log(level: .debug, "➡️ Event: \(event.eventName)")
            let chatEvent = ChatEvent(rawValue: event.eventName)
            let data = Data(event.data?.utf8 ?? "".utf8)
            
            switch chatEvent {
            case .message:
                self?.handleMessageEvent(data: data)
            case .attachmentsUpdated:
                self?.handleAttachmentEvent(data: data)
            case .takeover:
                self?.handleHandoverEvent(data: data)
            case .subscriptionSucceeded:
                Logger.shared.log(level: .debug, "Subscribed to Pusher events")
            case .none:
                Logger.shared.log(level: .debug, "Unhandled event: \(event.eventName)")
            }
        }
    }
    
    private func handleMessageEvent(data: Data) {
        do {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = ChatMessageDTO.dateFormat
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(dateFormatter)

            let message = try decoder.decode(ChatMessageDTO.self, from: data).toChatMessage()
            DispatchQueue.main.async {
                // Don't add possible duplicates.
                if self.messages.contains(message) {
                    return
                }
                self.messages.append(message)
            }
        } catch {
            Logger.shared.log(level: .error, "Failed to decode chat message: \(error)")
        }
    }

    private func handleAttachmentEvent(data: Data) {
        do {
            let dto = try JSONDecoder().decode(ReceivedAttachmentDTO.self, from: data)
            dto.attachments.forEach { attachmentDTO in
                let attachment = attachmentDTO.toAttachment()
                let chatMessage = attachment.toMesssage(chatMessageID: dto.chatMessageId)
                DispatchQueue.main.async {
                    self.messages.append(chatMessage)
                }
            }
        } catch {
            Logger.shared.log(level: .error, "Failed to decode attachment data: \(error)")
        }
    }
    
    private func handleHandoverEvent(data: Data) {
        do {
            let takeoverRequest = try JSONDecoder().decode(TakeoverDTO.self, from: data)
            if takeoverRequest.takenOver {
                // Handle handover
                // Not in the scope for version 1.
            }
        } catch {
            Logger.shared.log(level: .error, "Failed to decode handover event: \(error)")
        }
    }
    
    // MARK: User events
    
    /// Send a message in the active chat.
    ///
    /// - Parameter message: ``UserMessage`` with content.
    /// - Throws: KindlyError.tokenMissing
    internal func sendMessage(_ message: UserMessage) async throws {
        var message = message
        
        guard let token = token else {
            throw KindlySDKError.tokenMissing
        }
        
        if let value = self.localContext {
            message.newContext = value
        }
        let data = try JSONEncoder().encode(message)

        let headers: [HTTPHeader] = [
            .kindlyChatBubbleToken(value: token),
            .contentType(value: "application/json")
        ]
        
        DispatchQueue.main.async { [message] in
            self.messages.append(message.toChatMessage())
        }
        
        try await httpClient.request(
            endpoint: .message,
            headers: headers,
            body: data
        )
        self.performAfterCallDuties()
    }
    
    /// Handle button selection in the chat.
    ///
    /// The documentation contains a list of supported button types.
    /// - Parameter buttonInteraction: ButtonInteractionDTO.
    internal func submitButtonSelection(buttonInteraction: ButtonInteractionDTO) async throws {
        var buttonInteraction = buttonInteraction
        
        guard let token = token else {
            throw KindlySDKError.tokenMissing
        }
        
        if let value = self.localContext {
            buttonInteraction.newContext = value
        }
        
        let headers: [HTTPHeader] = [
            .kindlyChatBubbleToken(value: token),
            .contentType(value: "application/json")
        ]
    
        do {
            let data = try JSONEncoder().encode(buttonInteraction)
            try await httpClient.request(endpoint: .buttonClick, headers: headers, body: data)
            self.performAfterCallDuties()
        } catch {
            Logger.shared.log(level: .debug, "Failed to submit button press: \(error.localizedDescription)")
            throw error
        }
    }
}

// MARK: Auth Request for authenticating with Pusher

@available(iOS 14.0, *)
private final class AuthRequestBuilder: AuthRequestBuilderProtocol {
    @Injected<KindlyChatClient> private var kindlyChatClient: KindlyChatClient

    func requestFor(socketID: String, channelName: String) -> URLRequest? {
        var request = URLRequest(url: API.url(for: .authPusher))
        request.httpMethod = HTTPMethod.POST.rawValue
        request.httpBody = "socket_id=\(socketID)&channel_name=\(channelName)".data(using: .utf8)
        guard let token = kindlyChatClient.token else {
            return nil
        }
        request.addValue(token, forHTTPHeaderField: "kindly-chatbubble-token")
        return request
    }
}

/// Connects to the Kindly API and starts a new chat session.
@available(iOS 14.0, *)
public extension KindlyChatClient {
    func connect(languageCode: LanguageCode? = nil, completion: @escaping (Result<Void, Error>) -> Void) {
        Task {
            do {
                try await connect(languageCode: languageCode)
            } catch {
                completion(.failure(error))
                return
            }
            
            // authenticate
            KeychainService.clientJWTToken.delete()
            authenticationState = .none
            try? await authenticateWithAuthToken(forceNew: true)
            
            completion(.success(()))
        }
    }
}
