import Foundation

internal struct TakeoverDTO: Decodable {
    let takenOver: Bool
    let name: String
    let avatarURL: String

    private enum CodingKeys: String, CodingKey {
        case takenOver = "taken_over"
        case name
        case avatarURL = "avatar"
    }
}
