import Foundation

/// Represents the values when interacting with buttons in the chat view.
internal struct ButtonInteractionDTO: Encodable {
    let button: ButtonClickDTO
    let chatMessage: ChatMessageDTO
    var newContext: [String: String]? = nil
    
    private enum CodingKeys: String, CodingKey {
        case button = "button"
        case chatMessage = "chatMessage"
        case newContext = "new_context"
    }
    
    struct ButtonClickDTO: Encodable {
        let id: String?
        let buttonType: ButtonType
        let value: String

        enum CodingKeys: String, CodingKey {
            case id
            case buttonType = "button_type"
            case value
        }
    }

    struct ChatMessageDTO: Encodable {
        let id: String
        let sender: ChatMessageSender
    }
}
