import Foundation

public struct ConnectRequestModel: Encodable {
    public var context: [String: String]? = nil
    
    private enum CodingKeys: String, CodingKey {
        case context = "context"
    }
}
