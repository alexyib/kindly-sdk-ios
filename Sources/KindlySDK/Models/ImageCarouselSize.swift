import Foundation

/// Kindly API returns an integer value for the image size.
/// The image size is set as `small`, `medium` or `large` in the admin panel, but a specific value is received.
/// Defaults to the `medium` image size.
internal enum ImageCarouselSize {
    case small
    case medium
    case large

    // Convert hardcoded values from backend to cases.
    init(size: Int?) {
        switch size {
        case 600: self = .small
        case 800: self = .medium
        case 1000: self = .large
        default: self = .medium
        }
    }

    /// The image size used in the chat view.
    var imageSize: Double {
        switch self {
        case .small: return 130
        case .medium: return 180
        case .large: return 260
        }
    }
}
