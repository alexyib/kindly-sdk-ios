import Foundation

/// Interactive buttons in the chat view.
internal struct ChatButton: Hashable {
    let id: String?
    let type: ButtonType
    let label: String
    let value: String?
    let index: Int?
    var hasBeenSelected: Bool
}

/// Supported button types.
internal enum ButtonType: String, Encodable {
    case abortFollowup = "abort_followup"
    case dialogueTrigger = "dialogue_trigger"
    case email
    case link
    case phone
    case quickReply = "quick_reply"
    case suggestionDialogueTrigger = "suggestion_dialogue_trigger"
    case takeoverRequest = "takeover_request"
    case unknown
    case uploadAttachment = "upload_attachment"
}
