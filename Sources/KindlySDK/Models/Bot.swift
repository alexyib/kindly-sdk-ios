import Foundation

/// A chat bot the user can interact with.
public struct Bot {
    // MARK: Public properties

    /// The display name for the bot.
    public let name: String
    /// URL for the bot avatar image.
    public let avatarURL: URL?

    // MARK: Internal properties

    /// Delay in milliseconds.
    internal let typingDuration: Int
    /// Bot should display a typing indicator before replying.
    internal let typingIndicator: Bool
}
