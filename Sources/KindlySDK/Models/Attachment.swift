import Foundation

/// Represents a sent or received attachment.
internal struct Attachment: Hashable {
    let id: String
    let name: String
    let format: FileFormat
    let status: AttachmentStatus
    let sender: ChatMessageSender
    let storagePath: String

    func toMesssage(chatMessageID: String) -> ChatMessage {
        ChatMessage(
            id: chatMessageID,
            text: nil,
            buttons: [],
            imagesCarousel: [],
            attachments: [self],
            imageCarouselSize: nil,
            exchangeType: nil,
            sender: sender,
            videoSource: nil,
            created: Date.gmt,
            exchangeID: nil,
            isFromUser: true
        )
    }
}

/// Status for an attachment.
internal enum AttachmentStatus: String, Encodable {
    /// Attachment validated by backend.
    case clean = "CLEAN"
    /// Not accepted.
    case error = "ERROR"
    /// Attachment in progress to be uploaded.
    case processing = "PROCESSING"
    /// Unknown status.
    case unknown
}
