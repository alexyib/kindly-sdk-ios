import Foundation
import UniformTypeIdentifiers

/// Supported file formats.
internal enum FileFormat: String, CaseIterable, Hashable {
    case jpeg
    case pdf
    case png
    case txt
    case unsupported

    /// Convert from Apple's UTType to our own FileFormat
    init(utType: String?) {
        switch utType {
        case "public.jpeg": self = .jpeg
        case "public.png": self = .png
        case "com.adobe.pdf": self = .pdf
        case "public.plain-text": self = .txt
        case "public.utf8-plain-text ": self = .txt
        default: self = .unsupported
        }
    }

    /// Convert from MIME type to our own FileFormat.
    init(mimeType: String?) {
        switch mimeType {
        case "image/jpeg": self = .jpeg
        case "application/pdf": self = .pdf
        case "image/png": self = .png
        case "text/*": self = .txt
        default: self = .unsupported
        }
    }

    /// Returns the file's MIME type. Returns an empty string if the file is unsupported.
    var mimeType: String {
        switch self {
        case .jpeg: return "image/jpeg"
        case .pdf: return "application/pdf"
        case .png: return "image/png"
        case .txt: return "text/*"
        case .unsupported: return ""
        }
    }

    /// Returns file extension.
    var fileExtension: String {
        return ".\(rawValue)"
    }
}

extension FileFormat {
    /// Returns the file's UTType.
    @available(iOS 14.0, *)
    var utType: UTType {
        switch self {
        case .jpeg: return .image
        case .pdf: return .pdf
        case .png: return .image
        case .txt: return .plainText
        case .unsupported: return .plainText
        }
    }
}
