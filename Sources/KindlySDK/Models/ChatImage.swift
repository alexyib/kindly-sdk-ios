import Foundation

/// Images displayed in the chat view.
internal struct ChatImage: Hashable {
    let imageURL: URL
    let title: String?
    let description: String?
    let altText: String?
    let linkURL: URL?
}
