import Foundation

/// ChatMessage represents both sent and received messages.
///
/// Messages supports a variety of different content. For example images, buttons and attachments.
internal struct ChatMessage: Hashable, Identifiable, Comparable {
    let id: String
    let text: String?
    let buttons: [ChatButton]
    let imagesCarousel: [ChatImage]
    let attachments: [Attachment]
    let imageCarouselSize: ImageCarouselSize?
    let exchangeType: ExchangeType?
    let sender: ChatMessageSender
    let videoSource: URL?
    let created: Date
    let exchangeID: String?
    let isFromUser: Bool

    static func < (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        lhs.created < rhs.created
    }
}

/// ExchangeType is a value from Kindly that represents what kind of exchange this is.
internal enum ExchangeType: String, Encodable {
    case dialogue
    case fallback
    case greeting
    case systemDialogue = "system_dialogue"
    case userSays = "user_says"
}

/// Defines who is a message's sender.
internal enum ChatMessageSender: String, Encodable {
    case agent = "AGENT"
    case bot = "BOT"
    case system = "SYSTEM"
    case unknown
    case user = "USER"

    /// Suggested alignment for messages in chat view.
    var messageAlignment: MessageAlignment {
        switch self {
        case .agent, .bot, .system, .unknown:
            return .received
        case .user:
            return .sent
        }
    }
}
