import Foundation

internal struct ConnectionDTO: Decodable {
    let chatID: String
    let token: String
    let allGreetingsHideInput: Bool
    let botName: String
    let botAvatar: String?
    let botTypingDuration: Int
    let botTypingIndicator: Bool

    private enum CodingKeys: String, CodingKey {
        case chatID = "chat_id"
        case token
        case allGreetingsHideInput = "all_greetings_hide_input"
        case botName = "bot_name"
        case botAvatar = "bot_avatar"
        case botTypingDuration = "bot_typing_duration"
        case botTypingIndicator = "bot_typing_indicator"
    }

    func toConnection() -> Connection {
        return Connection(
            chatID: chatID,
            token: token,
            allGreetingsHideInput: allGreetingsHideInput,
            bot: Bot(
                name: botName,
                avatarURL: URL(string: botAvatar ?? ""),
                typingDuration: botTypingDuration,
                typingIndicator: botTypingIndicator
            )
        )
    }
}
