import Foundation

/// A message created by the user.
///
/// Timestamp with current time will automatically be added.
public struct UserMessage: Encodable {
    /// Required
    public let message: String
    public var newContext: [String: String]? = nil
    
    private enum CodingKeys: String, CodingKey {
        case message = "message"
        case newContext = "new_context"
    }
}

extension UserMessage {
    func toChatMessage() -> ChatMessage {
        return ChatMessage(
            id: UUID().uuidString,
            text: message,
            buttons: [],
            imagesCarousel: [],
            attachments: [],
            imageCarouselSize: nil,
            exchangeType: .dialogue,
            sender: .user,
            videoSource: nil,
            created: Date.gmt,
            exchangeID: nil,
            isFromUser: true
        )
    }
}
