import Foundation

/// Represents a connection with the Kindly API.
///
/// ``token`` is required for making additional API calls, like sending messages and more.
internal struct Connection {
    let chatID: String
    let token: String
    let allGreetingsHideInput: Bool
    let bot: Bot
}
