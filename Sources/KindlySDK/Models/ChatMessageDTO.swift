import Foundation

internal struct ChatMessageDTO: Decodable {
    let id: String
    let message: String
    let buttons: [ChatButtonDTO]?
    let imageCarousel: [ChatImageDTO]?
    let imageCarouselSize: Int?
    let exchangeType: String?
    let sender: String?
    let videoSource: String?
    let created: Date
    let exchangeID: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case message
        case buttons
        case imageCarousel = "image_carousel"
        case imageCarouselSize = "image_carousel_size"
        case exchangeType = "exchange_type"
        case sender
        case videoSource = "video_source"
        case created
        case exchangeID = "exchange_id"
    }
    
    static let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    
    func toChatMessage() -> ChatMessage {
        return ChatMessage(
            id: id,
            text: message,
            buttons: buttons?.map { button in button.toChatButton() } ?? [],
            imagesCarousel: imageCarousel?.map { image in image.toChatImage() } ?? [],
            attachments: [],
            imageCarouselSize: .init(size: imageCarouselSize),
            exchangeType: ExchangeType(rawValue: exchangeType ?? ""),
            sender: ChatMessageSender(rawValue: sender ?? "") ?? .unknown,
            videoSource: URL(string: videoSource ?? ""),
            created: created,
            exchangeID: exchangeID,
            isFromUser: false
        )
    }
}

internal struct ChatButtonDTO: Decodable {
    let id: String?
    let buttonType: String
    let label: String
    let value: String?
    let index: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case buttonType = "button_type"
        case label
        case value
        case index
    }
    
    func toChatButton() -> ChatButton {
        return ChatButton(
            id: id,
            type: ButtonType(rawValue: buttonType) ?? .unknown,
            label: label,
            value: value,
            index: index,
            hasBeenSelected: false
        )
    }
}

internal struct ChatImageDTO: Decodable {
    let imageURL: String
    let title: String?
    let description: String?
    let altText: String?
    let linkURL: String?
    
    private enum CodingKeys: String, CodingKey {
        case imageURL = "imageUrl"
        case title
        case description
        case altText
        case linkURL = "linkUrl"
    }
    
    func toChatImage() -> ChatImage {
        return ChatImage(
            imageURL: URL(string: imageURL)!,
            title: title,
            description: description,
            altText: altText,
            linkURL: URL(string: linkURL ?? "")
        )
    }
}
