# Kindly SDK for iOS

Integrate Kindly Chat in your iOS app. 

* This SDK makes it easy to add chat to your app.
* Provides a chat UI that can be branded.    
* Made for iPhone and iPad.

![Screenshot](/Sources/KindlySDK/Documentation.docc/Resources/screenshot-main.png)

## 📦 Installation

### Requirements

- iOS deployment target: 14.0+
- Swift Package Manager

### How to install

#### Swift Package Manager

Add the GitHub Repo URL to your Xcode project. 
_In the Xcode menu bar, select "File" -> "Add Packages..."_

```
https://github.com/kindly-ai/chat-sdk-ios
```

## 📋 Getting started

### Documentation

- Here you'll find the documentation and getting started guide.
- The SDK is documented with DocC.

[↗️ Kindly SDK for iOS Documentation](https://kindly-ai.github.io/chat-sdk-ios/documentation/kindlysdk/)

### Demo App

- Available in the Xcode project `KindlySDKDemoApp`.
- Insert API config details in `AppDelegate.swift`. 
- The demo app provides UIKit and SwiftUI examples. 

## Credits

Kindly SDK for iOS implements the following dependencies:

* [PusherSwift](https://github.com/pusher/pusher-websocket-swift)
* [SwiftyGif](https://github.com/kirualex/SwiftyGif)
